﻿using System;

namespace Infrastructure.Common.Exceptions
{
    /// <summary>
    ///     Исключение, возникающее в случае, если не удалось найти указанный объект.
    /// </summary>
    public class ObjectNotFoundException : Exception
    {
    }
}