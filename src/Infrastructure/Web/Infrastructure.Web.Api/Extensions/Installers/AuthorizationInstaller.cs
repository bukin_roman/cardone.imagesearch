﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Web.Api.Extensions.Installers
{
    public static class AuthorizationInstaller
    {
        public static IServiceCollection InstallAuthorization(
            this IServiceCollection services,
            Action<AuthorizationOptions> configure = null)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            if (configure != null)
                services.AddAuthorization(configure);
            else
                services.AddAuthorization();

            return services;
        }
    }
}