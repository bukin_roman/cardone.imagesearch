﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Web.Api.Extensions.Installers
{
    public static class LocalizationInstaller
    {
        public static IServiceCollection InstallLocalization(
            this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddLocalization();
            services.Configure<RequestLocalizationOptions>(options =>
                {
                    options.AddSupportedCultures("ru-RU");
                    options.AddSupportedUICultures("ru-RU");
                    options.SetDefaultCulture("ru-RU");
                    options.FallBackToParentCultures = false;
                    options.FallBackToParentUICultures = false;
                }
            );

            return services;
        }
    }
}