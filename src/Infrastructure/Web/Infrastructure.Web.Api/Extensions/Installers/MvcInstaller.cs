﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Infrastructure.Web.Api.Mvc.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using ProblemDetails = Infrastructure.Web.Api.Mvc.ProblemDetails;

namespace Infrastructure.Web.Api.Extensions.Installers
{
    public static class MvcInstaller
    {
        private static readonly Assembly CurrentAssembly = typeof(MvcInstaller).Assembly;

        public static IServiceCollection InstallMvcForApi(
            this IServiceCollection services,
            Action<MvcOptions> configure,
            params Assembly[] assembliesToScanForApplicationParts)
        {
            // like full MVC
            // https://github.com/aspnet/Mvc/blob/2.1.3/src/Microsoft.AspNetCore.Mvc/MvcServiceCollectionExtensions.cs#L27
            if (services == null) throw new ArgumentNullException(nameof(services));
            var builder = services.AddMvcCore();
            builder.AddApiExplorer();
            builder.AddAuthorization();
            builder.AddFormatterMappings();
            builder.AddDataAnnotations();
            builder.AddJsonFormatters();

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add<OperationCanceledExceptionFilter>();
                // Swagger
                options.Filters.Add(new ProducesResponseTypeAttribute(typeof(ProblemDetails), 400));
                configure?.Invoke(options);
            });

            builder.SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // Custom
            builder.AddJsonOptions(options =>
            {
                options.AllowInputFormatterExceptionMessages = false;
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.Converters.Add(
                    new StringEnumConverter
                    {
                        AllowIntegerValues = true,
                        NamingStrategy = new CamelCaseNamingStrategy()
                    });
            });
            var assemblies = GetAssemblies(assembliesToScanForApplicationParts);
            if (assemblies != null)
                foreach (var assemblyToScanForApplicationPart in assemblies)
                    builder.AddApplicationPart(assemblyToScanForApplicationPart);

            services.Configure<RouteOptions>(options =>
            {
                options.LowercaseUrls = true;
                options.AppendTrailingSlash = false;
            });
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressMapClientErrors = true;
                options.SuppressUseValidationProblemDetailsForInvalidModelStateResponses = false;
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ProblemDetails(context.ModelState)
                    {
                        Status = StatusCodes.Status400BadRequest
                    };
                    var traceId = Activity.Current?.Id ?? context.HttpContext.TraceIdentifier;
                    problemDetails.Extensions["traceId"] = traceId;
                    problemDetails.Title = "Validation";

                    var result = new BadRequestObjectResult(problemDetails);

                    result.ContentTypes.Add("application/problem+json");
                    result.ContentTypes.Add("application/problem+xml");

                    return result;
                };
            });
            services.AddHttpContextAccessor();
            return services;
        }


        private static Assembly[] GetAssemblies(Assembly[] assembliesToScanForApplicationParts)
        {
            var notNullAssemblies = assembliesToScanForApplicationParts
                                        ?.Distinct()
                                        .ToArray()
                                    ?? new[] {CurrentAssembly};
            if (!notNullAssemblies.Contains(CurrentAssembly))
            {
                var assemblies = new List<Assembly>(notNullAssemblies) {CurrentAssembly};
                return assemblies.ToArray();
            }

            return notNullAssemblies;
        }
    }
}