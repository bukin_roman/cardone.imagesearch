﻿using System;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Web.Api.Extensions.Installers
{
    public static class CorsInstaller
    {
        public static IServiceCollection InstallCorsServices(
            this IServiceCollection services,
            string[] allowedDomains,
            string corsPolicyName = "DefaultCorsPolicy")
        {
            var policy = BuildCorsPolicy(allowedDomains ?? new string[] { });
            services.AddCors();
            services.Configure<CorsOptions>(options =>
            {
                options.AddPolicy(corsPolicyName, policy);
                options.DefaultPolicyName = corsPolicyName;
            });
            return services;
        }

        private static CorsPolicy BuildCorsPolicy(
            string[] allowedDomains)
        {
            var builder = new CorsPolicyBuilder(allowedDomains)
                .WithHeaders("accept", "content-type", "origin", "authorization", "x-requested-with")
                .WithMethods("GET", "POST", "PUT", "DELETE")
                .SetPreflightMaxAge(TimeSpan.FromSeconds(86400));
            var policy = builder.Build();
            return policy;
        }
    }
}