﻿using System;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Web.Api.Extensions.Installers
{
    public static class ClientAuthenticationInstaller
    {
        public static IServiceCollection InstallClientAuthentication(
            this IServiceCollection services,
            IConfiguration identityServerAuthenticationOptions)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services
                .AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    identityServerAuthenticationOptions.Bind(options);
                    options.NameClaimType = "sub";
                    options.RoleClaimType = "role";
                });

            return services;
        }
    }
}