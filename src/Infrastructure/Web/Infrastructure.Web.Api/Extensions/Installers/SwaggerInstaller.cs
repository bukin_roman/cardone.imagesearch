﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Infrastructure.Web.Api.Swagger.Filters;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Infrastructure.Web.Api.Extensions.Installers
{
    public static class SwaggerInstaller
    {
        private static readonly Assembly CurrentAssembly = typeof(SwaggerInstaller).Assembly;

        public static IServiceCollection InstallSwagger(
            this IServiceCollection services,
            string servicePath,
            string serviceTitle,
            string serviceVersion,
            Assembly[] docsAssemblies,
            OAuth2Scheme oauth2Scheme = null,
            Action<SwaggerGenOptions> configure = null)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            var assemblies = GetAssembliesForSwagger(docsAssemblies);

            var docFiles = GetXmlDocFiles(assemblies);
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(
                    servicePath,
                    new Info
                    {
                        Title = serviceTitle,
                        Version = serviceVersion
                    });

                foreach (var docFile in docFiles)
                    options.IncludeXmlComments(docFile);
                options.EnableAnnotations();
                options.ExampleFilters();
                options.OperationFilter<SwaggerDefaultValues>();
                if (oauth2Scheme != null)
                    ApplySecurity(options, oauth2Scheme);
                configure?.Invoke(options);
            });

            services.AddSwaggerExamples();
            services.Scan(scan =>
                scan.FromAssemblies(assemblies)
                    // IExamplesProvider<>
                    .AddClasses(classes => classes.AssignableTo(typeof(IExamplesProvider<>)))
                    .AsImplementedInterfaces()
                    .WithSingletonLifetime()
                    // IExamplesProvider
                    .AddClasses(classes => classes.AssignableTo(typeof(IExamplesProvider)))
                    .AsSelf()
                    .WithSingletonLifetime());

            return services;
        }

        private static void ApplySecurity(SwaggerGenOptions options, OAuth2Scheme oauth2Scheme)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));
            if (oauth2Scheme == null)
                throw new ArgumentNullException(nameof(oauth2Scheme));
            options.AddSecurityDefinition("oauth2", oauth2Scheme);
            var scopes = oauth2Scheme.Scopes.Keys.Distinct().OrderBy(x => x).ToArray();
            options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
            {
                {"oauth2", scopes}
            });
        }

        private static Assembly[] GetAssembliesForSwagger(params Assembly[] optionalAssemblies)
        {
            var assemblies = optionalAssemblies?.Distinct().ToArray() ?? new Assembly[] { };
            if (assemblies.Contains(CurrentAssembly))
                return assemblies;
            return new List<Assembly>(assemblies)
                {
                    CurrentAssembly
                }
                .ToArray();
        }

        private static string[] GetXmlDocFiles(Assembly[] assembliesForXmlDocs)
        {
            var docFiles = assembliesForXmlDocs
                .Select(GetDocFile)
                .Where(x => !string.IsNullOrEmpty(x))
                .Distinct()
                .OrderBy(x => x)
                .ToArray();
            return docFiles;
        }

        private static string GetDocFile(Assembly assembly)
        {
            var assemblyFile = new FileInfo(assembly.Location);
            if (assemblyFile.Exists)
            {
                var assemblyDirectory = assemblyFile.Directory;
                if (assemblyDirectory != null && assemblyDirectory.Exists)
                {
                    var xmlDocFile = new FileInfo(
                        Path.Combine(
                            assemblyDirectory.FullName,
                            Path.GetFileNameWithoutExtension(assemblyFile.Name) + ".xml"));
                    if (xmlDocFile.Exists)
                        return xmlDocFile.FullName;
                }
            }

            return null;
        }
    }
}