﻿using System.Text;

namespace Infrastructure.Web.Api.Extensions
{
    public static class CodePages
    {
        public static void Register()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }
    }
}