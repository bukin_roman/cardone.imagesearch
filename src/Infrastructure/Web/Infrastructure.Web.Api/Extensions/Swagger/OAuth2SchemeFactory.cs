﻿using System;
using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;

namespace Infrastructure.Web.Api.Extensions.Swagger
{
    public static class OAuth2SchemeFactory
    {
        public static OAuth2Scheme Create(
            string authorizationServerLocation,
            string scopeName,
            string scopeDescription)
        {
            if (string.IsNullOrEmpty(authorizationServerLocation))
                throw new ArgumentNullException(nameof(authorizationServerLocation));
            if (string.IsNullOrEmpty(scopeName))
                throw new ArgumentNullException(nameof(scopeName));
            if (string.IsNullOrEmpty(scopeDescription))
                throw new ArgumentNullException(nameof(scopeDescription));

            var identityServerUri = new Uri(authorizationServerLocation, UriKind.Absolute);
            var authorizationUri = new Uri(
                    identityServerUri,
                    new Uri("/connect/authorize", UriKind.Relative))
                .ToString();
            var tokenUri = new Uri(
                    identityServerUri,
                    new Uri("/connect/token", UriKind.Relative))
                .ToString();
            return new OAuth2Scheme
            {
                Flow = "implicit",
                AuthorizationUrl = authorizationUri,
                TokenUrl = tokenUri,
                Scopes = new Dictionary<string, string>
                {
                    {scopeName, scopeDescription}
                }
            };
        }
    }
}