﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Infrastructure.Web.Api.Mvc
{
    public class ProblemDetails : ValidationProblemDetails
    {
        public ProblemDetails()
        {
        }

        public ProblemDetails(ModelStateDictionary modelState) : base(modelState)
        {
        }

        public ProblemDetails(IDictionary<string, string[]> errors) : base(errors)
        {
        }
    }
}