﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace Infrastructure.Web.Api.Mvc.Controllers
{
    /// <summary>
    ///     Контроллер для обработки ошибок.
    /// </summary>
    [AllowAnonymous]
    [Route("error")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ControllerBase
    {
        public IActionResult Index()
        {
            string lastExceptionMessage = null;
            var feature = HttpContext.Features.Get<IExceptionHandlerFeature>();
            if (feature?.Error != null) lastExceptionMessage = GetExceptionMessage(feature.Error);

            var traceId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            var problem = new ProblemDetails
            {
                Title = "Exception",
                Detail = lastExceptionMessage
            };
            problem.Extensions["traceId"] = traceId;
            var result = new BadRequestObjectResult(problem);
            return result;
        }

        private string GetExceptionMessage(Exception exception)
        {
            var lastException = UnrollException(exception);
            return lastException.Message;
        }

        private Exception UnrollException(Exception exception)
        {
            var currentException = exception;
            while (currentException.InnerException != null)
                currentException = currentException.InnerException;
            return currentException;
        }
    }
}