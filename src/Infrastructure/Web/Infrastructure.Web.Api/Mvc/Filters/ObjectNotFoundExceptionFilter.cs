﻿using Infrastructure.Common.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Infrastructure.Web.Api.Mvc.Filters
{
    /// <summary>
    ///     Фильтр для исключения <see cref="ObjectNotFoundException" />. В случае возникновения данного исключения -
    ///     возвращает 404.
    /// </summary>
    public class ObjectNotFoundExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (!(context.Exception is ObjectNotFoundException)) return;
            context.ExceptionHandled = true;
            context.Result = new StatusCodeResult(404);
        }
    }
}