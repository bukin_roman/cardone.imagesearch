﻿using System;
using Infrastructure.Web.Api.Properties;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Web.Api.Mvc.Filters
{
    /// <summary>
    ///     Фильтр исключений для отменённых запросов.
    /// </summary>
    public class OperationCanceledExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogger _logger;

        public OperationCanceledExceptionFilter(ILoggerFactory loggerFactory)
        {
            if (loggerFactory == null) throw new ArgumentNullException(nameof(loggerFactory));
            _logger = loggerFactory.CreateLogger<OperationCanceledExceptionFilter>();
        }

        public override void OnException(ExceptionContext context)
        {
            if (!(context.Exception is OperationCanceledException)) return;
            _logger.LogInformation(Resources.RequestWasCancelled);
            context.ExceptionHandled = true;
            context.Result = new StatusCodeResult(499);
        }
    }
}