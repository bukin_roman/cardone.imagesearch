﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using Infrastructure.Specifications.BooleanOperators;

namespace Infrastructure.Specifications
{
    /// <summary>
    ///     Базовый класс для спецификаций, которые могут быть скомбинированы путём использования логических операторов AND, OR
    ///     или NOT.
    /// </summary>
    /// <typeparam name="T">Тип объекта, для которого создаётся спецификация.</typeparam>
    public abstract class Specification<T>
    {
        /// <summary>
        ///     Возвращает LINQ-выражение, описывающее спецификацию.
        /// </summary>
        /// <returns></returns>
        public abstract Expression<Func<T, bool>> ToExpression();

        /// <summary>
        ///     Возвращает строку, представляющую спецификацию.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ToExpression()?.ToString() ?? string.Empty;
        }

        /// <summary>
        ///     Выполняет неявное преобразование <see cref="Specification{T}" /> в LINQ-выражение.
        /// </summary>
        /// <param name="spec"></param>
        public static implicit operator Expression<Func<T, bool>>(Specification<T> spec)
        {
            return spec.ToExpression();
        }

        /// <summary>
        ///     Перегрузка оператора <see langword="false" /> для поддержки операций && и ||.
        /// </summary>
        /// <param name="spec">Спецификация, для которой перегружается оператор.</param>
        /// <returns></returns>
        [SuppressMessage("ReSharper", "UnusedParameter.Global")]
        public static bool operator false(Specification<T> spec)
        {
            return false;
        }

        /// <summary>
        ///     Перегрузка оператора <see langword="true" /> для поддержки операций && и ||.
        /// </summary>
        /// <param name="spec">Спецификация, для которой перегружается оператор.</param>
        /// <returns></returns>
        [SuppressMessage("ReSharper", "UnusedParameter.Global")]
        public static bool operator true(Specification<T> spec)
        {
            return false;
        }

        /// <summary>
        ///     Позволяет комбинировать две спецификации, используя логическое AND.
        /// </summary>
        /// <param name="spec1">Первая комбинируемая спецификация.</param>
        /// <param name="spec2">Вторая комбинируемая спецификация.</param>
        /// <returns></returns>
        public static Specification<T> operator &(Specification<T> spec1, Specification<T> spec2)
        {
            return new AndSpecification<T>(spec1, spec2);
        }

        /// <summary>
        ///     Позволяет комбинировать две спецификации, используя логическое OR.
        /// </summary>
        /// <param name="spec1">Первая комбинируемая спецификация.</param>
        /// <param name="spec2">Вторая комбинируемая спецификация.</param>
        /// <returns></returns>
        public static Specification<T> operator |(Specification<T> spec1, Specification<T> spec2)
        {
            return new OrSpecification<T>(spec1, spec2);
        }

        /// <summary>
        ///     Создаёт спецификацию, отрицающую значение указанной спецификации, реализующую логическое NOT для указанной
        ///     спецификации.
        /// </summary>
        /// <param name="spec">Спецификация, для которой будет реализовано логическое NOT.</param>
        /// <returns></returns>
        public static Specification<T> operator !(Specification<T> spec)
        {
            return new NotSpecification<T>(spec);
        }
    }
}