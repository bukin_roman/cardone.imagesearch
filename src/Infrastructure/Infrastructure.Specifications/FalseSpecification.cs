﻿using System;
using System.Linq.Expressions;

namespace Infrastructure.Specifications
{
    /// <summary>
    ///     Спецификация, которая НЕ будет удовлетворять любому объекту.
    /// </summary>
    /// <typeparam name="T">Тип объекта, для которого создаётся спецификация.</typeparam>
    public class FalseSpecification<T> : Specification<T>
    {
        /// <inheritdoc />
        public override Expression<Func<T, bool>> ToExpression()
        {
            return x => false;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return GetType() == other.GetType();
        }

        public override int GetHashCode()
        {
            return GetType().GetHashCode();
        }
    }
}