﻿using System;
using System.Linq.Expressions;

namespace Infrastructure.Specifications
{
    /// <summary>
    ///     Класс, позволяющий реализовывать <see cref="Specification{T}" /> без написания класса-наследника, реализующего
    ///     спецификацию.
    /// </summary>
    /// <typeparam name="T">Тип объекта, для которого создаётся спецификация.</typeparam>
    public class AdHocSpecification<T> : Specification<T>
    {
        /// <summary>
        ///     Предикат, представляющий выражение спецификации.
        /// </summary>
        private readonly Expression<Func<T, bool>> _predicate;

        /// <summary>
        ///     Конструирует спецификацию.
        /// </summary>
        /// <param name="predicate">Предикат, который будет представлять выражение спецификации.</param>
        public AdHocSpecification(Expression<Func<T, bool>> predicate)
        {
            _predicate = predicate ?? throw new ArgumentNullException(nameof(predicate));
        }

        /// <inheritdoc />
        public override Expression<Func<T, bool>> ToExpression()
        {
            return _predicate;
        }
    }
}