﻿using FluentMigrator.Runner.VersionTableInfo;

namespace Infrastructure.DataAccess.PostgreSQL.FluentMigrator
{
    [VersionTableMetaData]
    public class PostgreSqlVersionTableMetadata : IVersionTableMetaData
    {
        public object ApplicationContext { get; set; }

        public bool OwnsSchema => true;

        public string SchemaName => "public";

        public string TableName => "_migrations";

        public string ColumnName => "version";

        public string DescriptionColumnName => "description";

        public string UniqueIndexName => "_migrations_version_idx";

        public string AppliedOnColumnName => "applied_on";
    }
}