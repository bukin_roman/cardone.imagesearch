﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.DataAccess.PostgreSQL.FluentMigrator.Extensions.Installers
{
    public static class FluentMigratorInstaller
    {
        private static readonly Assembly CurrentAssembly = typeof(FluentMigratorInstaller).Assembly;

        public static IServiceCollection InstallPostgresFluentMigrator(
            this IServiceCollection services,
            string connectionString,
            params Assembly[] assemblies)
        {
            var processingAssemblies = GetFluentMigratorAssemblies(assemblies);
            services
                .AddFluentMigratorCore()
                .ConfigureRunner(options => options
                    .AddPostgres()
                    .WithGlobalConnectionString(connectionString)
                    .ScanIn(processingAssemblies).For.All()
                )
                .AddSingleton<IAssemblySourceItem>(new AssemblySourceItem(processingAssemblies));
            return services;
        }

        private static Assembly[] GetFluentMigratorAssemblies(params Assembly[] optionalAssemblies)
        {
            var assemblies = optionalAssemblies?.Distinct().Where(x => x != null).ToArray() ?? new Assembly[] { };
            if (assemblies.Contains(CurrentAssembly))
                return assemblies;
            return new List<Assembly>(assemblies)
                {
                    CurrentAssembly
                }
                .ToArray();
        }
    }
}