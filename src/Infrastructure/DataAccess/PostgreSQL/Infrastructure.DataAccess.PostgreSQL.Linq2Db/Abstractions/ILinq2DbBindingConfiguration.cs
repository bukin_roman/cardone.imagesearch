﻿using LinqToDB.Mapping;

namespace Infrastructure.DataAccess.PostgreSQL.Linq2Db.Abstractions
{
    public interface ILinq2DbBindingConfiguration
    {
        void Configure(FluentMappingBuilder builder);
    }
}