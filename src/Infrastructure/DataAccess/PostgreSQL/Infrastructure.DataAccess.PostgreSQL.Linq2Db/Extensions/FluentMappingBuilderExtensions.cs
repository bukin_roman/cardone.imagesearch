﻿using System;
using Infrastructure.DataAccess.PostgreSQL.Linq2Db.Abstractions;
using LinqToDB.Mapping;

namespace Infrastructure.DataAccess.PostgreSQL.Linq2Db.Extensions
{
    public static class FluentMappingBuilderExtensions
    {
        public static FluentMappingBuilder Apply(
            this FluentMappingBuilder builder,
            ILinq2DbBindingConfiguration configuration)
        {
            if (builder == null)
                throw new ArgumentNullException(nameof(builder));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            configuration.Configure(builder);
            return builder;
        }
    }
}