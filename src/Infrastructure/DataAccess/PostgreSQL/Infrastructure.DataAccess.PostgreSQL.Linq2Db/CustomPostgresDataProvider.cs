﻿using System;
using System.Reflection;
using LinqToDB.Data;
using LinqToDB.DataProvider.PostgreSQL;

namespace Infrastructure.DataAccess.PostgreSQL.Linq2Db
{
    public class CustomPostgresDataProvider : PostgreSQLDataProvider
    {
        public const string ProviderName = "PostgreSQL.9.5.15";

        private static readonly Func<PostgreSQLDataProvider, string, string, bool> TryAddType =
            (Func<PostgreSQLDataProvider, string, string, bool>)
            typeof(CustomPostgresDataProvider)
                .BaseType
                .GetMethod("TryAddType", BindingFlags.Instance | BindingFlags.NonPublic)
                .CreateDelegate(typeof(Func<PostgreSQLDataProvider, string, string, bool>));

        public CustomPostgresDataProvider() : base(
            ProviderName,
            PostgreSQLVersion.v95)
        {
        }

        public static void Register()
        {
            var provider = new CustomPostgresDataProvider();
            DataConnection.AddDataProvider(provider);
        }

        protected override void OnConnectionTypeCreated(Type connectionType)
        {
            base.OnConnectionTypeCreated(connectionType);
            TryAddType(this, "varchar varying", "Varchar");
        }
    }
}