﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.AutoMapper.Extensions.Installers
{
    /// <summary>
    ///     Методы расширения для установки и настройки Automapper.
    /// </summary>
    public static class AutomapperInstaller
    {
        /// <summary>
        ///     Устанавливает и настраивает Automapper.
        /// </summary>
        /// <param name="services">Экземпляр настраиваемой коллекции сервисов.</param>
        /// <param name="typesToScanAssemblies">
        ///     Типы, сборки которых необходимо просканировать для сервисов, которые может
        ///     использовать Automapper (например, профилей маппингов).
        /// </param>
        /// <returns></returns>
        public static IServiceCollection InstallAutomapper(
            this IServiceCollection services,
            params Type[] typesToScanAssemblies)
        {
            var types = GetTypes(typesToScanAssemblies);
            services.AddAutoMapper(types);
            return services;
        }

        private static Type[] GetTypes(Type[] types)
        {
            var nonNullUniqueTypes = types?.Where(x => x != null).ToArray() ?? new Type[] { };
            var typeList = new List<Type>(nonNullUniqueTypes) {typeof(AutomapperInstaller)};
            var resultTypes = typeList.Distinct().ToArray();
            return resultTypes;
        }
    }
}