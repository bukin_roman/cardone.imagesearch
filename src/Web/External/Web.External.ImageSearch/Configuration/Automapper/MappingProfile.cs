﻿using AutoMapper;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService.Model;
using Web.External.ImageSearch.WebModels.GoogleApiSettings.Requests;
using Web.External.ImageSearch.WebModels.GoogleApiSettings.Responses;

namespace Web.External.ImageSearch.Configuration.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<GoogleApiSettings, GoogleApiSettingsResponseModel>();
            CreateMap<CreateGoogleApiSettingsRequestModel, GoogleApiSettingsCreateModel>();
            CreateMap<UpdateGoogleApiSettingsRequestModel, GoogleApiSettingsUpdateModel>();
        }
    }
}