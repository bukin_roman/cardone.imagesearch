﻿namespace Web.External.ImageSearch.Configuration
{
    public static class Constants
    {
        public static class Identity
        {
            public const string Scope = "api_image_search";

            public const string ScopeDescription = "Доступ к API поиска изображений";
        }
    }
}