﻿using System.Threading.Tasks;
using Hangfire.Server;

namespace Web.External.ImageSearch.Hangfire.Abstractions
{
    /// <summary>
    ///     Интерфейс асинхронной повторяющейся задачи для Hangfire.
    /// </summary>
    public interface IAsyncRepeatableHangfireJob
    {
        /// <summary>
        ///     Возвращает наименование задачи.
        /// </summary>
        /// <returns></returns>
        string GetName();

        /// <summary>
        ///     Возвращает выражение Cron, описывающее интервал повторения задачи.
        /// </summary>
        /// <returns></returns>
        string GetCronExpression();

        /// <summary>
        ///     Асинхронно запускает выполнение указанной задачи.
        /// </summary>
        /// <param name="context">Контекст выполнения задачи.</param>
        /// <returns></returns>
        Task RunJob(PerformContext context);
    }
}