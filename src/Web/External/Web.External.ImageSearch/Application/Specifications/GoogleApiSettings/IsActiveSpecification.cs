﻿using System;
using System.Linq.Expressions;
using Infrastructure.Specifications;

namespace Web.External.ImageSearch.Application.Specifications.GoogleApiSettings
{
    public class IsActiveSpecification : Specification<Model.GoogleApiSettings>
    {
        private readonly DateTimeOffset _maximalFailDate;

        public IsActiveSpecification(DateTimeOffset maximalFailDate)
        {
            _maximalFailDate = maximalFailDate;
        }

        public override Expression<Func<Model.GoogleApiSettings, bool>> ToExpression()
        {
            var maximalFailDate = _maximalFailDate;
            return x => x.DailyRequestLimit > 0 && (x.LastFailDate == null || x.LastFailDate < maximalFailDate);
        }
    }
}