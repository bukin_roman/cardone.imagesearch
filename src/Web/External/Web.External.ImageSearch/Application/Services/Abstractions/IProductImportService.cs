﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Web.External.ImageSearch.Application.Model;

namespace Web.External.ImageSearch.Application.Services.Abstractions
{
    /// <summary>
    ///     Сервис для импорта товаров.
    /// </summary>
    public interface IProductImportService
    {
        /// <summary>
        ///     Асинхронно импортирует товары (выполняет сохранение, либо обновление существующих товаров).
        /// </summary>
        /// <param name="products">Список импортируемых товаров.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.ч</param>
        /// <returns></returns>
        Task ImportProductsAsync(
            IEnumerable<Product> products,
            CancellationToken cancellationToken = default);
    }
}