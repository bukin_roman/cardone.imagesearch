﻿using System.Threading;
using System.Threading.Tasks;

namespace Web.External.ImageSearch.Application.Services.Abstractions.SearchService
{
    /// <summary>
    ///     Сервис для поиска в Google.
    /// </summary>
    public interface IGoogleSearchService
    {
        Task<SearchRequestResponse> FindImagesAsync(
            string searchRequest,
            CancellationToken cancellationToken = default);
    }
}