﻿using Web.External.ImageSearch.Application.Model;

namespace Web.External.ImageSearch.Application.Services.Abstractions.SearchService
{
    public interface IGoogleSearchServiceFactory
    {
        IGoogleSearchService Create(GoogleApiSettings settings);
    }
}