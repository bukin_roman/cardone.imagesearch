﻿using System;

namespace Web.External.ImageSearch.Application.Services.Abstractions.SearchService
{
    /// <summary>
    ///     Ответ на поисковой запрос.
    /// </summary>
    public class SearchRequestResponse
    {
        private SearchRequestResponse(bool isSucceeded, string[] imagesUris)
        {
            IsSucceeded = isSucceeded;
            ImagesUris = imagesUris;
        }

        /// <summary>
        ///     Был ли запрос успешным.
        /// </summary>
        public bool IsSucceeded { get; }

        /// <summary>
        ///     Ссылки на изображения.
        /// </summary>
        public string[] ImagesUris { get; }

        public static SearchRequestResponse Ok(string[] imagesUris)
        {
            if (imagesUris == null)
                throw new ArgumentNullException(nameof(imagesUris));
            return new SearchRequestResponse(true, imagesUris);
        }

        public static SearchRequestResponse Failed()
        {
            return new SearchRequestResponse(false, new string[] { });
        }
    }
}