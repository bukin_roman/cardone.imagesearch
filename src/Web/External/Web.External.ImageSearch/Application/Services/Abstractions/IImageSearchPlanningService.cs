﻿using System.Threading;
using System.Threading.Tasks;

namespace Web.External.ImageSearch.Application.Services.Abstractions
{
    /// <summary>
    ///     Сервис планирования поиска изображений.
    /// </summary>
    public interface IImageSearchPlanningService
    {
        /// <summary>
        ///     Планирует поиск изображений.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены выполнения операции.</param>
        /// <returns></returns>
        Task PlanSearchAsync(CancellationToken cancellationToken = default);
    }
}