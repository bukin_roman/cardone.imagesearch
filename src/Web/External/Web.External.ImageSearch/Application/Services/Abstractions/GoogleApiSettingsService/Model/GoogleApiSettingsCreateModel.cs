﻿using Web.External.ImageSearch.Application.Model;

namespace Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService.Model
{
    /// <summary>
    ///     Модель для создания <see cref="GoogleApiSettings" />.
    /// </summary>
    public class GoogleApiSettingsCreateModel : BaseGoogleApiSettingsOperationalModel
    {
        /// <summary>
        ///     Конструирует модель для создания <see cref="GoogleApiSettings" />.
        /// </summary>
        /// <param name="customSearchEngineId">Идентификатор Custom Search Engine.</param>
        /// <param name="googleApiKey">Ключ Google API.</param>
        /// <param name="dailyRequestLimit">Ограничение на количество поисковых запросов в сутки.</param>
        public GoogleApiSettingsCreateModel(
            string customSearchEngineId,
            string googleApiKey,
            int dailyRequestLimit)
            : base(
                customSearchEngineId,
                googleApiKey,
                dailyRequestLimit)
        {
        }
    }
}