﻿using Web.External.ImageSearch.Application.Model;

namespace Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService.Model
{
    /// <summary>
    ///     Модель для обновления <see cref="GoogleApiSettings" />.
    /// </summary>
    public class GoogleApiSettingsUpdateModel : BaseGoogleApiSettingsOperationalModel
    {
        /// <summary>
        ///     Конструирует модель для обновления <see cref="GoogleApiSettings" />.
        /// </summary>
        /// <param name="customSearchEngineId">Идентификатор Custom Search Engine.</param>
        /// <param name="googleApiKey">Ключ Google API.</param>
        /// <param name="dailyRequestLimit">Ограничение на количество поисковых запросов в сутки.</param>
        public GoogleApiSettingsUpdateModel(
            string customSearchEngineId,
            string googleApiKey,
            int dailyRequestLimit)
            : base(
                customSearchEngineId,
                googleApiKey,
                dailyRequestLimit)
        {
        }
    }
}