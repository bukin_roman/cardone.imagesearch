﻿using System;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Properties;

namespace Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService.Model
{
    /// <summary>
    ///     Базовая модель для выполнения различных операций над <see cref="GoogleApiSettings" />
    /// </summary>
    public abstract class BaseGoogleApiSettingsOperationalModel
    {
        /// <summary>
        ///     Конструирует базовую модель для выполнения различных операций над <see cref="GoogleApiSettings" />.
        /// </summary>
        /// <param name="customSearchEngineId">Идентификатор Custom Search Engine.</param>
        /// <param name="googleApiKey">Ключ Google API.</param>
        /// <param name="dailyRequestLimit">Ограничение на количество поисковых запросов в сутки.</param>
        protected BaseGoogleApiSettingsOperationalModel(
            string customSearchEngineId,
            string googleApiKey,
            int dailyRequestLimit)
        {
            if (string.IsNullOrEmpty(customSearchEngineId))
                throw new ArgumentNullException(nameof(customSearchEngineId));
            if (customSearchEngineId.Length > 255)
                throw new ArgumentException(
                    string.Format(Resources.StringArgument_LengthError, nameof(customSearchEngineId), 255),
                    nameof(customSearchEngineId));
            if (string.IsNullOrEmpty(googleApiKey))
                throw new ArgumentNullException(nameof(googleApiKey));
            if (googleApiKey.Length > 255)
                throw new ArgumentException(
                    string.Format(Resources.StringArgument_LengthError, nameof(googleApiKey), 255),
                    nameof(googleApiKey));
            if (dailyRequestLimit < 0)
                throw new ArgumentOutOfRangeException(nameof(dailyRequestLimit));

            CustomSearchEngineId = customSearchEngineId;
            GoogleApiKey = googleApiKey;
            DailyRequestLimit = dailyRequestLimit;
        }

        /// <summary>
        ///     Идентификатор Custom Search Engine.
        /// </summary>
        public string CustomSearchEngineId { get; }

        /// <summary>
        ///     Ключ Google API.
        /// </summary>
        public string GoogleApiKey { get; }

        /// <summary>
        ///     Ограничение на количество поисковых запросов в сутки.
        /// </summary>
        public int DailyRequestLimit { get; }
    }
}