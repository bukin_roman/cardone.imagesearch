﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService.Model;

namespace Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService
{
    /// <summary>
    ///     Сервис для работы с настройками для взаимодействия с Google API.
    /// </summary>
    public interface IGoogleApiSettingsService
    {
        /// <summary>
        ///     Возвращает все существующие настройки.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены выполнения запроса.</param>
        /// <returns></returns>
        Task<IEnumerable<GoogleApiSettings>> GetAllAsync(CancellationToken cancellationToken = default);

        /// <summary>
        ///     Ищет настройки по уникальному идентификатору.
        /// </summary>
        /// <param name="id">Уникальный идентификатор настроек.</param>
        /// <param name="cancellationToken">Токен отмены выполнения запроса.</param>
        /// <returns></returns>
        Task<GoogleApiSettings> FindAsync(
            long id,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Создаёт новые настройки.
        /// </summary>
        /// <param name="model">Модель для создания <see cref="GoogleApiSettings" />.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.</param>
        /// <returns></returns>
        Task<GoogleApiSettings> CreateAsync(
            GoogleApiSettingsCreateModel model,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Обновляет существующие настройки.
        /// </summary>
        /// <param name="id">Уникальный идентификатор обновляемых настроек.</param>
        /// <param name="model">Модель для обновления настроек.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.</param>
        /// <returns></returns>
        Task<GoogleApiSettings> UpdateAsync(
            long id,
            GoogleApiSettingsUpdateModel model,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Удаляет настройки по их уникальному идентификатору.
        /// </summary>
        /// <param name="id">Уникальный идентификатор удаляемых настроек.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.</param>
        /// <returns></returns>
        Task DeleteAsync(
            long id,
            CancellationToken cancellationToken = default);
    }
}