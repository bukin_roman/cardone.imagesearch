﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Data;
using Web.External.ImageSearch.Application.DataAccess;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Model.Temp;
using Web.External.ImageSearch.Application.Services.Abstractions;

namespace Web.External.ImageSearch.Application.Services.Implementations
{
    public class ProductImportService : IProductImportService
    {
        private readonly ImageSearchConnection _connection;

        public ProductImportService(ImageSearchConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }

        public async Task ImportProductsAsync(
            IEnumerable<Product> products,
            CancellationToken cancellationToken = default)
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products));
            cancellationToken.ThrowIfCancellationRequested();

            using (var transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var tempTable = CopyProductsIntoTempTable(products, cancellationToken);
                await UpsertProductsFromTempTableAsync(tempTable, cancellationToken);
                await tempTable.DropAsync(false, cancellationToken);
                transaction.Commit();
            }
        }

        private ITable<TempProduct> CopyProductsIntoTempTable(
            IEnumerable<Product> products,
            CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var tempProductsTableName = _connection.TempProducts.TableName
                                        + "_"
                                        + DateTimeOffset.UtcNow.Ticks.ToString("D", CultureInfo.InvariantCulture);

            var tempProductsTable = _connection.CreateTable<TempProduct>(tempProductsTableName);
            var tempProductsToInsert = products.Select(x => new TempProduct
            {
                ProductId = x.ProductId,
                VendorCode = x.VendorCode,
                ManufacturerName = x.ManufacturerName,
                Title = x.Title
            });
            tempProductsTable.BulkCopy(
                new BulkCopyOptions
                {
                    BulkCopyType = BulkCopyType.ProviderSpecific,
                    CheckConstraints = true
                },
                tempProductsToInsert);

            return tempProductsTable;
        }

        private async Task UpsertProductsFromTempTableAsync(
            ITable<TempProduct> tempProducts,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var sourceTable = (tempProducts.SchemaName + "." + tempProducts.TableName).Trim('.');
            var targetTable = (_connection.Products.SchemaName + "." + _connection.Products.TableName).Trim('.');

            var sql = $"INSERT INTO {targetTable} (product_id, vendor_code, manufacturer_name, title) " +
                      $"SELECT product_id, vendor_code, manufacturer_name, title FROM {sourceTable} " +
                      "ON CONFLICT (product_id) DO UPDATE SET vendor_code = EXCLUDED.vendor_code, manufacturer_name = EXCLUDED.manufacturer_name, title = EXCLUDED.title;";
            await _connection.ExecuteAsync(sql, cancellationToken);
        }
    }
}