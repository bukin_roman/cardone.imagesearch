﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Common.Exceptions;
using LinqToDB;
using Web.External.ImageSearch.Application.DataAccess;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService;
using Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService.Model;

namespace Web.External.ImageSearch.Application.Services.Implementations
{
    public class GoogleApiSettingsService : IGoogleApiSettingsService
    {
        private readonly ImageSearchConnection _connection;

        public GoogleApiSettingsService(ImageSearchConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<GoogleApiSettings>> GetAllAsync(
            CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            return await _connection.GoogleApiSettings
                .OrderBy(x => x.Id)
                .ToListAsync(cancellationToken);
        }

        /// <inheritdoc />
        public async Task<GoogleApiSettings> FindAsync(
            long id,
            CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (id <= 0)
                throw new ArgumentOutOfRangeException(nameof(id));
            return await _connection.GoogleApiSettings
                .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        /// <inheritdoc />
        public async Task<GoogleApiSettings> CreateAsync(
            GoogleApiSettingsCreateModel model,
            CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            using (var transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var insertedId = await _connection
                    .GoogleApiSettings
                    .InsertWithInt64IdentityAsync(() => new GoogleApiSettings
                    {
                        CustomSearchEngineId = model.CustomSearchEngineId,
                        GoogleApiKey = model.GoogleApiKey,
                        DailyRequestLimit = model.DailyRequestLimit
                    }, cancellationToken);
                var insertedSettings = await _connection
                    .GoogleApiSettings
                    .SingleOrDefaultAsync(x => x.Id == insertedId, cancellationToken);
                transaction.Commit();
                return insertedSettings;
            }
        }

        /// <inheritdoc />
        public async Task<GoogleApiSettings> UpdateAsync(
            long id,
            GoogleApiSettingsUpdateModel model,
            CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (id <= 0)
                throw new ArgumentOutOfRangeException(nameof(id));
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            using (var transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var updatedRecords = await _connection
                    .GoogleApiSettings
                    .UpdateAsync(
                        x => x.Id == id,
                        x => new GoogleApiSettings
                        {
                            CustomSearchEngineId = model.CustomSearchEngineId,
                            GoogleApiKey = model.GoogleApiKey,
                            DailyRequestLimit = model.DailyRequestLimit
                        },
                        cancellationToken);
                if (updatedRecords < 1)
                    throw new ObjectNotFoundException();
                var updatedSettings = await _connection
                    .GoogleApiSettings
                    .SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
                if (updatedSettings == null)
                    throw new ObjectNotFoundException();
                transaction.Commit();
                return updatedSettings;
            }
        }

        /// <inheritdoc />
        public async Task DeleteAsync(long id, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (id <= 0)
                throw new ArgumentOutOfRangeException(nameof(id));
            using (var transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var deletedRecordsCount = await _connection
                    .GoogleApiSettings
                    .DeleteAsync(x => x.Id == id, cancellationToken);
                if (deletedRecordsCount < 1)
                    throw new ObjectNotFoundException();
                transaction.Commit();
            }
        }
    }
}