﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Data;
using Web.External.ImageSearch.Application.DataAccess;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Model.Enums;
using Web.External.ImageSearch.Application.Services.Abstractions;
using Web.External.ImageSearch.Application.Specifications.GoogleApiSettings;

namespace Web.External.ImageSearch.Application.Services.Implementations
{
    public class ImageSearchPlanningService : IImageSearchPlanningService
    {
        private readonly ImageSearchConnection _connection;

        public ImageSearchPlanningService(ImageSearchConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }

        public async Task PlanSearchAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            using (var transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                if (!await IsAnyProductsToSearchExists(cancellationToken))
                    return;

                var settings = await GetAllActiveSettingsAsync(cancellationToken);
                var summaryLimit = settings.Sum(x => x.DailyRequestLimit);
                if (settings.Count == 0 || summaryLimit == 0)
                    return;
                var pendingProducts = await GetProductsToSearch(summaryLimit, cancellationToken);
                if (pendingProducts.Count == 0)
                    return;
                var balancedProducts = BalanceProducts(settings, pendingProducts);
                CreateSearchTasks(balancedProducts, cancellationToken);
                transaction.Commit();
            }
        }

        private async Task<bool> IsAnyProductsToSearchExists(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var countOfProductsWithoutImages = await _connection
                .Products
                .LongCountAsync(x => x.Images.LongCount() == 0L, cancellationToken);
            return countOfProductsWithoutImages > 0L;
        }

        private async Task<List<GoogleApiSettings>> GetAllActiveSettingsAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var maximalFailDate = DateTimeOffset.UtcNow.AddDays(-1);
            var settings = await _connection
                .GoogleApiSettings
                .OrderBy(x => x.Id)
                .Where(new IsActiveSpecification(maximalFailDate))
                .ToListAsync(cancellationToken);
            return settings;
        }

        private async Task<List<Product>> GetProductsToSearch(
            int limit,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var products = await _connection
                .Products
                .Where(x => x.Images.LongCount() == 0L && x.SearchTasks.LongCount() == 0L)
                .OrderBy(x => x.CreationDate)
                .Take(limit)
                .ToListAsync(cancellationToken);
            return products;
        }

        private Dictionary<GoogleApiSettings, List<Product>> BalanceProducts(
            List<GoogleApiSettings> settings,
            List<Product> products)
        {
            if (settings.Count < 1)
                throw new ArgumentOutOfRangeException(nameof(settings));
            var result = new Dictionary<GoogleApiSettings, List<Product>>();
            var limits = new Dictionary<GoogleApiSettings, int>();
            foreach (var setting in settings)
            {
                if (setting.DailyRequestLimit < 1)
                    throw new ArgumentOutOfRangeException(nameof(setting.DailyRequestLimit));
                result.Add(setting, new List<Product>(setting.DailyRequestLimit));
                limits.Add(setting, setting.DailyRequestLimit);
            }

            var index = 0;
            var keys = limits.Keys.ToList();
            foreach (var product in products)
            {
                var currentSettings = keys[index];
                result[currentSettings].Add(product);
                limits[currentSettings] -= 1;
                if (limits[currentSettings] == 0)
                {
                    limits.Remove(currentSettings);
                    keys.Remove(currentSettings);
                }
                else
                {
                    index++;
                }

                if (index == limits.Keys.Count)
                    index = 0;
            }

            return result;
        }

        private void CreateSearchTasks(
            Dictionary<GoogleApiSettings, List<Product>> balancedProducts,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var searchTasks = balancedProducts
                .SelectMany(x => x.Value.Select(v => new {Settings = x.Key, Product = v}))
                .Select(x => new SearchTask
                {
                    GoogleApiSettingsId = x.Settings.Id,
                    ProductId = x.Product.ProductId,
                    Status = SearchTaskStatus.Planned
                });
            _connection.SearchTasks.BulkCopy(new BulkCopyOptions
            {
                BulkCopyType = BulkCopyType.ProviderSpecific,
                CheckConstraints = true
            }, searchTasks);
        }
    }
}