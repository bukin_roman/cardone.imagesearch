﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Newtonsoft.Json;
using Web.External.ImageSearch.Application.Services.Abstractions.SearchService;

namespace Web.External.ImageSearch.Application.Services.Implementations.SearchService
{
    public class GoogleSearchService : IGoogleSearchService
    {
        private readonly string _apiKey;
        private readonly string _customSearchEngineId;
        private readonly HttpClient _httpClient;

        public GoogleSearchService(
            string apiKey,
            string customSearchEngineId,
            HttpClient httpClient)
        {
            _apiKey = apiKey;
            _customSearchEngineId = customSearchEngineId;
            _httpClient = httpClient;
        }

        public async Task<SearchRequestResponse> FindImagesAsync(
            string searchRequest,
            CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            try
            {
                var uri = BuildSearchRequestRelativeUri(searchRequest);
                using (var response = await _httpClient.GetAsync(
                    uri,
                    HttpCompletionOption.ResponseHeadersRead,
                    cancellationToken))
                {
                    var intStatusCode = (int) response.StatusCode;
                    if (intStatusCode == 200)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();
                        var internalModel = JsonConvert
                            .DeserializeObject<InternalJsonResponse>(jsonString);
                        var links = internalModel
                            ?.Items
                            ?.Select(x => x.Link)
                            .Where(x => !string.IsNullOrEmpty(x))
                            .ToArray();
                        if (links != null) return SearchRequestResponse.Ok(links);
                    }
                }
            }
            catch (Exception)
            {
                return SearchRequestResponse.Failed();
            }

            return SearchRequestResponse.Failed();
        }


        private Uri BuildSearchRequestRelativeUri(string searchRequest)
        {
            var queryBuilder = new QueryBuilder
            {
                {"searchType", "image"},
                {"cx", _customSearchEngineId},
                {"key", _apiKey},
                {"q", searchRequest}
            };
            var query = queryBuilder.ToQueryString().ToUriComponent();
            var relativeUri = new Uri(
                "customsearch/v1" + query.ToString(CultureInfo.InvariantCulture),
                UriKind.Relative);
            return relativeUri;
        }

        private class InternalJsonResponse
        {
            public InternalJsonResponseItem[] Items { get; set; }
        }

        private class InternalJsonResponseItem
        {
            public string Link { get; set; }
        }
    }
}