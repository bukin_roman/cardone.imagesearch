﻿using System;
using System.Net.Http;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Services.Abstractions.SearchService;

namespace Web.External.ImageSearch.Application.Services.Implementations.SearchService
{
    public class GoogleSearchServiceFactory : IGoogleSearchServiceFactory
    {
        private readonly HttpClient _httpClient;

        public GoogleSearchServiceFactory(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public IGoogleSearchService Create(GoogleApiSettings settings)
        {
            if (settings == null)
                throw new ArgumentNullException(nameof(settings));
            return new GoogleSearchService(
                settings.GoogleApiKey,
                settings.CustomSearchEngineId,
                _httpClient);
        }
    }
}