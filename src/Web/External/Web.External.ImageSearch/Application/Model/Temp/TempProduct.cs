﻿using LinqToDB;
using LinqToDB.Mapping;

namespace Web.External.ImageSearch.Application.Model.Temp
{
    [Table(Schema = "public", Name = "temp_product")]
    public class TempProduct
    {
        [Column("product_id", DbType = "bigint", DataType = DataType.Int64, Precision = 64, Scale = 0)]
        [PrimaryKey]
        [NotNull]
        public long ProductId { get; set; }

        [Column("vendor_code", DbType = "character varying(255)", DataType = DataType.NVarChar, Length = 255)]
        [NotNull]
        public string VendorCode { get; set; }

        [Column("manufacturer_name", DbType = "character varying(128)", DataType = DataType.Undefined, Length = 128)]
        [NotNull]
        public string ManufacturerName { get; set; }

        [Column("title", DbType = "character varying(4000)", DataType = DataType.Undefined, Length = 4000)]
        [NotNull]
        public string Title { get; set; }
    }
}