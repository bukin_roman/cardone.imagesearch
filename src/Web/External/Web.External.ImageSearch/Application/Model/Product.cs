﻿using System;
using System.Collections.Generic;
using LinqToDB;
using LinqToDB.Mapping;

namespace Web.External.ImageSearch.Application.Model
{
    [Table(Schema = "public", Name = "product")]
    public class Product
    {
        [Column(
            "product_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [PrimaryKey]
        [NotNull]
        public long ProductId { get; set; }

        [Column(
            "vendor_code",
            DbType = "character varying(255)",
            DataType = DataType.NVarChar,
            Length = 255)]
        [NotNull]
        public string VendorCode { get; set; }

        [Column(
            "manufacturer_name",
            DbType = "character varying(128)",
            DataType = DataType.NVarChar,
            Length = 128)]
        [NotNull]
        public string ManufacturerName { get; set; }

        [Column(
            "title",
            DbType = "character varying(4000)",
            DataType = DataType.NVarChar,
            Length = 4000)]
        [NotNull]
        public string Title { get; set; }

        [Column(
            "creation_date",
            DbType = "timestamp (6) with time zone",
            DataType = DataType.DateTimeOffset,
            Precision = 6,
            SkipOnInsert = true,
            SkipOnUpdate = true)]
        [NotNull]
        public DateTimeOffset CreationDate { get; set; }

        #region Associations

        [Association(
            ThisKey = nameof(ProductId),
            OtherKey = nameof(ProductImage.ProductId),
            CanBeNull = true,
            Relationship = Relationship.OneToMany,
            IsBackReference = true)]
        public IEnumerable<ProductImage> Images { get; set; }

        [Association(
            ThisKey = nameof(ProductId),
            OtherKey = nameof(SearchTask.ProductId),
            CanBeNull = true,
            Relationship = Relationship.OneToMany,
            IsBackReference = true)]
        public IEnumerable<SearchTask> SearchTasks { get; set; }

        [Association(
            ThisKey = nameof(ProductId),
            OtherKey = nameof(SelectedProductImage.ProductId),
            CanBeNull = true,
            Relationship = Relationship.OneToOne,
            IsBackReference = true)]
        public SelectedProductImage SelectedImage { get; set; }

        #endregion
    }
}