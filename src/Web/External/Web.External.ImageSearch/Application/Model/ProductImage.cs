﻿using System.Collections.Generic;
using LinqToDB;
using LinqToDB.Mapping;

namespace Web.External.ImageSearch.Application.Model
{
    [Table(Schema = "public", Name = "product_image")]
    public class ProductImage
    {
        [Column(
            "id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [PrimaryKey]
        [Identity]
        public long Id { get; set; }

        [Column(
            "search_task_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [Nullable]
        public long? SearchTaskId { get; set; }

        [Column(
            "product_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [NotNull]
        public long ProductId { get; set; }

        [Column(
            "url",
            DbType = "character varying(4000)",
            DataType = DataType.NVarChar,
            Length = 4000)]
        [NotNull]
        public string Url { get; set; }

        #region Associations

        [Association(
            ThisKey = nameof(ProductId),
            OtherKey = nameof(Model.Product.ProductId),
            CanBeNull = false,
            Relationship = Relationship.ManyToOne,
            KeyName = "product_image_product_id_fkey",
            BackReferenceName = nameof(Model.Product.Images))]
        public Product Product { get; set; }

        [Association(
            ThisKey = nameof(SearchTaskId),
            OtherKey = nameof(Model.SearchTask.Id),
            CanBeNull = true,
            Relationship = Relationship.ManyToOne,
            KeyName = "product_image_search_task_id_fkey",
            BackReferenceName = nameof(Model.SearchTask.FoundImages))]
        public SearchTask SearchTask { get; set; }

        [Association(
            ThisKey = nameof(Id),
            OtherKey = nameof(SelectedProductImage.SelectedImageId),
            CanBeNull = true,
            Relationship = Relationship.OneToMany,
            IsBackReference = true)]
        public IEnumerable<SelectedProductImage> SelectedProductImages { get; set; }

        #endregion
    }
}