﻿using System;
using LinqToDB;
using LinqToDB.Mapping;

namespace Web.External.ImageSearch.Application.Model
{
    [Table(Schema = "public", Name = "selected_product_image")]
    public class SelectedProductImage
    {
        [Column(
            "product_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [PrimaryKey]
        [NotNull]
        public long ProductId { get; set; }

        [Column(
            "selected_image_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [NotNull]
        public long SelectedImageId { get; set; }

        [Column(
            "author_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [Nullable]
        public long? AuthorId { get; set; }

        [Column(
            "creation_date",
            DbType = "timestamp (6) with time zone",
            DataType = DataType.DateTimeOffset,
            Precision = 6,
            SkipOnInsert = true,
            SkipOnUpdate = true)]
        [NotNull]
        public DateTimeOffset CreationDate { get; set; }

        #region Associations

        [Association(
            ThisKey = nameof(ProductId),
            OtherKey = nameof(Model.Product.ProductId),
            CanBeNull = false,
            Relationship = Relationship.OneToOne,
            KeyName = "selected_product_image_product_id_fkey",
            BackReferenceName = nameof(Model.Product.SelectedImage))]
        public Product Product { get; set; }

        [Association(
            ThisKey = nameof(SelectedImageId),
            OtherKey = nameof(Model.ProductImage.Id),
            CanBeNull = false,
            Relationship = Relationship.ManyToOne,
            KeyName = "selected_product_image_selected_image_id_fkey",
            BackReferenceName = nameof(Model.ProductImage.SelectedProductImages))]
        public ProductImage ProductImage { get; set; }

        #endregion
    }
}