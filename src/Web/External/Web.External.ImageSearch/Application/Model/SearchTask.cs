﻿using System;
using System.Collections.Generic;
using LinqToDB;
using LinqToDB.Mapping;
using Web.External.ImageSearch.Application.Model.Enums;

namespace Web.External.ImageSearch.Application.Model
{
    [Table(Schema = "public", Name = "search_task")]
    public class SearchTask
    {
        [Column(
            "id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [PrimaryKey]
        [Identity]
        public long Id { get; set; }

        [Column(
            "google_api_settings_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [NotNull]
        public long GoogleApiSettingsId { get; set; }

        [Column(
            "product_id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [NotNull]
        public long ProductId { get; set; }

        [Column(
            "last_execution_attempt_date",
            DbType = "timestamp (6) with time zone",
            DataType = DataType.DateTimeOffset,
            Precision = 6,
            SkipOnInsert = true)]
        [Nullable]
        public DateTimeOffset? LastExecutionAttemptDate { get; set; }

        [Column(
            "status",
            DbType = "integer",
            DataType = DataType.Int32,
            Precision = 32,
            Scale = 0)]
        [NotNull]
        public SearchTaskStatus Status { get; set; }

        [Column(
            "creation_date",
            DbType = "timestamp (6) with time zone",
            DataType = DataType.DateTimeOffset,
            Precision = 6,
            SkipOnInsert = true,
            SkipOnUpdate = true)]
        [NotNull]
        public DateTimeOffset CreationDate { get; set; }

        #region Associations

        [Association(
            ThisKey = nameof(GoogleApiSettingsId),
            OtherKey = nameof(Model.GoogleApiSettings.Id),
            CanBeNull = false,
            Relationship = Relationship.ManyToOne,
            KeyName = "search_task_google_api_settings_id_fkey",
            BackReferenceName = nameof(Model.GoogleApiSettings.SearchTasks))]
        public GoogleApiSettings GoogleApiSettings { get; set; }

        [Association(
            ThisKey = nameof(ProductId),
            OtherKey = nameof(Model.Product.ProductId),
            CanBeNull = false,
            Relationship = Relationship.ManyToOne,
            KeyName = "search_task_product_id_fkey",
            BackReferenceName = nameof(Model.Product.SearchTasks))]
        public Product Product { get; set; }

        [Association(
            ThisKey = nameof(Id),
            OtherKey = nameof(ProductImage.SearchTaskId),
            CanBeNull = true,
            Relationship = Relationship.OneToMany,
            IsBackReference = true)]
        public IEnumerable<ProductImage> FoundImages { get; set; }

        #endregion
    }
}