﻿namespace Web.External.ImageSearch.Application.Model.Enums
{
    public enum SearchTaskStatus
    {
        Planned = 0,
        Completed = 1,
        Failed = 2
    }
}