﻿using System;
using System.Collections.Generic;
using LinqToDB;
using LinqToDB.Mapping;

namespace Web.External.ImageSearch.Application.Model
{
    [Table(Schema = "public", Name = "google_api_settings")]
    public class GoogleApiSettings
    {
        [Column(
            "id",
            DbType = "bigint",
            DataType = DataType.Int64,
            Precision = 64,
            Scale = 0)]
        [PrimaryKey]
        [Identity]
        public long Id { get; set; }

        [Column(
            "custom_search_engine_id",
            DbType = "character varying(255)",
            DataType = DataType.NVarChar,
            Length = 255)]
        [NotNull]
        public string CustomSearchEngineId { get; set; }

        [Column(
            "google_api_key",
            DbType = "character varying(255)",
            DataType = DataType.NVarChar,
            Length = 255)]
        [NotNull]
        public string GoogleApiKey { get; set; }

        [Column(
            "daily_request_limit",
            DbType = "integer",
            DataType = DataType.Int32,
            Precision = 32,
            Scale = 0)]
        [NotNull]
        public int DailyRequestLimit { get; set; }

        [Column(
            "last_fail_date",
            DbType = "timestamp (6) with time zone",
            DataType = DataType.DateTimeOffset,
            Precision = 6,
            SkipOnInsert = true)]
        [Nullable]
        public DateTimeOffset? LastFailDate { get; set; }

        [Column(
            "creation_date",
            DbType = "timestamp (6) with time zone",
            DataType = DataType.DateTimeOffset,
            Precision = 6,
            SkipOnInsert = true,
            SkipOnUpdate = true)]
        [NotNull]
        public DateTimeOffset CreationDate { get; set; }

        #region Associations

        [Association(
            ThisKey = nameof(Id),
            OtherKey = nameof(SearchTask.GoogleApiSettingsId),
            CanBeNull = true,
            Relationship = Relationship.OneToMany,
            IsBackReference = true)]
        public IEnumerable<SearchTask> SearchTasks { get; set; }

        #endregion
    }
}