﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hangfire.Server;
using LinqToDB;
using Web.External.ImageSearch.Application.DataAccess;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Model.Enums;
using Web.External.ImageSearch.Application.Services.Abstractions.SearchService;
using Web.External.ImageSearch.Application.Specifications.GoogleApiSettings;

namespace Web.External.ImageSearch.Application.Jobs.FireAndForget
{
    public class SearchImagesBackgroundJob
    {
        private readonly ImageSearchConnection _connection;
        private readonly IGoogleSearchServiceFactory _factory;

        public SearchImagesBackgroundJob(ImageSearchConnection connection, IGoogleSearchServiceFactory factory)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        public async Task SearchImagesAsync(long googleApiSettingsId, PerformContext context)
        {
            var cancellationToken = context?.CancellationToken?.ShutdownToken ?? default;
            var operationDate = DateTimeOffset.UtcNow;
            cancellationToken.ThrowIfCancellationRequested();
            var apiSettings = await GetApiSettingsAsync(googleApiSettingsId, cancellationToken);
            var isActiveFunc = new IsActiveSpecification(DateTimeOffset.UtcNow.AddDays(-1)).ToExpression().Compile();
            var isApiSettingsActive = isActiveFunc(apiSettings);
            if (!isApiSettingsActive)
                return;
            var pendingTasks = await GetPendingTasksAsync(apiSettings, cancellationToken);
            if (pendingTasks.Count == 0)
                return;
            var searchService = _factory.Create(apiSettings);
            await ExecuteSearchTasksAsync(
                searchService,
                apiSettings,
                pendingTasks,
                operationDate,
                cancellationToken);
        }

        private async Task<GoogleApiSettings> GetApiSettingsAsync(
            long googleApiSettingsId,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var apiSettings = await _connection
                .GoogleApiSettings
                .SingleAsync(x => x.Id == googleApiSettingsId, cancellationToken);
            return apiSettings;
        }

        private async Task<List<SearchTask>> GetPendingTasksAsync(
            GoogleApiSettings googleApiSettings,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var pendingTasks = await _connection
                .SearchTasks
                .LoadWith(x => x.Product)
                .Where(x => x.Status == SearchTaskStatus.Planned
                            || x.Status == SearchTaskStatus.Failed
                            && x.GoogleApiSettingsId == googleApiSettings.Id)
                .OrderBy(x => x.CreationDate)
                .Take(googleApiSettings.DailyRequestLimit)
                .ToListAsync(cancellationToken);
            return pendingTasks;
        }

        private async Task ExecuteSearchTasksAsync(
            IGoogleSearchService searchService,
            GoogleApiSettings settings,
            List<SearchTask> searchTasks,
            DateTimeOffset operationDate,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            using (var transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var currentTaskIndex = 0;
                try
                {
                    for (; currentTaskIndex < searchTasks.Count; currentTaskIndex++)
                    {
                        var currentTask = searchTasks[currentTaskIndex];

                        var searchResult = await FindImagesAsync(
                            searchService,
                            currentTask,
                            cancellationToken);
                        SearchTaskStatus resultStatus;
                        if (searchResult.IsSucceeded)
                        {
                            foreach (var imageUri in searchResult.ImagesUris)
                                await _connection.ProductImages.InsertAsync(
                                    () => new ProductImage
                                    {
                                        SearchTaskId = currentTask.Id,
                                        ProductId = currentTask.ProductId,
                                        Url = imageUri
                                    }, cancellationToken);

                            resultStatus = SearchTaskStatus.Completed;
                        }
                        else
                        {
                            resultStatus = SearchTaskStatus.Failed;
                        }

                        // search
                        await _connection.SearchTasks.UpdateAsync(
                            x => x.Id == currentTask.Id,
                            x => new SearchTask
                            {
                                LastExecutionAttemptDate = operationDate,
                                Status = resultStatus
                            },
                            cancellationToken);
                    }
                }
                catch (Exception)
                {
                    await _connection
                        .GoogleApiSettings
                        .UpdateAsync(
                            x => x.Id == settings.Id,
                            x => new GoogleApiSettings
                            {
                                LastFailDate = operationDate
                            },
                            cancellationToken);
                    var failedTasksIds = new List<long>();
                    for (; currentTaskIndex < searchTasks.Count; currentTaskIndex++)
                        failedTasksIds.Add(searchTasks[currentTaskIndex].Id);

                    await _connection.SearchTasks.UpdateAsync(
                        x => failedTasksIds.Contains(x.Id),
                        x => new SearchTask
                        {
                            LastExecutionAttemptDate = operationDate,
                            Status = SearchTaskStatus.Failed
                        },
                        cancellationToken);
                }

                transaction.Commit();
            }
        }

        private async Task<SearchRequestResponse> FindImagesAsync(
            IGoogleSearchService searchService,
            SearchTask searchTask,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var product = searchTask?.Product;
            if (product == null)
                return SearchRequestResponse.Failed();
            var searchRequest = $"{product.ManufacturerName} {product.VendorCode}".Trim();
            return await searchService.FindImagesAsync(searchRequest, cancellationToken);
        }
    }
}