﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Server;
using LinqToDB;
using Web.External.ImageSearch.Application.DataAccess;
using Web.External.ImageSearch.Application.Jobs.FireAndForget;
using Web.External.ImageSearch.Application.Model.Enums;
using Web.External.ImageSearch.Hangfire.Abstractions;

namespace Web.External.ImageSearch.Application.Jobs.Repeatable
{
    public class PlannedSearchSchedulingForExecutionJob : IAsyncRepeatableHangfireJob
    {
        private readonly ImageSearchConnection _connection;
        private readonly IBackgroundJobClient _jobClient;

        public PlannedSearchSchedulingForExecutionJob(ImageSearchConnection connection, IBackgroundJobClient jobClient)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
            _jobClient = jobClient ?? throw new ArgumentNullException(nameof(jobClient));
        }

        public string GetCronExpression()
        {
            return Cron.MinuteInterval(30);
        }

        public string GetName()
        {
            return "Run Search";
        }

        public async Task RunJob(PerformContext context)
        {
            var cancellationToken = context?.CancellationToken?.ShutdownToken ?? default;
            List<long> settingsIdsToSearch;
            using (var transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                settingsIdsToSearch = await _connection
                    .SearchTasks
                    .Where(x => x.Status == SearchTaskStatus.Planned || x.Status == SearchTaskStatus.Failed)
                    .Select(x => x.GoogleApiSettingsId)
                    .Distinct()
                    .OrderBy(x => x)
                    .ToListAsync(cancellationToken);
                transaction.Commit();
            }

            if (settingsIdsToSearch.Count == 0)
                return;
            foreach (var settingsId in settingsIdsToSearch)
                _jobClient.Enqueue<SearchImagesBackgroundJob>(x => x.SearchImagesAsync(settingsId, null));
        }
    }
}