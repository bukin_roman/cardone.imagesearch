﻿using System;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Server;
using Web.External.ImageSearch.Application.Services.Abstractions;
using Web.External.ImageSearch.Hangfire.Abstractions;

namespace Web.External.ImageSearch.Application.Jobs.Repeatable
{
    public class ImageSearchPlanningJob : IAsyncRepeatableHangfireJob
    {
        private readonly IImageSearchPlanningService _planningService;

        public ImageSearchPlanningJob(IImageSearchPlanningService planningService)
        {
            _planningService = planningService ?? throw new ArgumentNullException(nameof(planningService));
        }

        public string GetCronExpression()
        {
            return Cron.MinuteInterval(10);
        }

        public string GetName()
        {
            return "Image Search Planing Job";
        }

        public async Task RunJob(PerformContext context)
        {
            var cancellationToken = context?.CancellationToken?.ShutdownToken ?? default;
            await _planningService.PlanSearchAsync(cancellationToken);
        }
    }
}