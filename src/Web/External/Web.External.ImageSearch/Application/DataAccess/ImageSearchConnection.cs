﻿using System;
using System.Diagnostics;
using Infrastructure.DataAccess.PostgreSQL.Linq2Db;
using LinqToDB;
using LinqToDB.Data;
using Microsoft.Extensions.Logging;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Model.Temp;

namespace Web.External.ImageSearch.Application.DataAccess
{
    public class ImageSearchConnection : DataConnection
    {
        private readonly ILogger<ImageSearchConnection> _logger;

        public ImageSearchConnection(
            string connectionString,
            ILogger<ImageSearchConnection> logger)
            : base(
                CustomPostgresDataProvider.ProviderName,
                connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

#if DEBUG
            TraceSwitch.Level = TraceLevel.Info;

            OnTraceConnection = info =>
            {
                LogLevel level;
                switch (info.TraceLevel)
                {
                    case TraceLevel.Error:
                    {
                        level = LogLevel.Error;
                        break;
                    }
                    case TraceLevel.Info:
                    {
                        level = LogLevel.Information;
                        break;
                    }
                    case TraceLevel.Off:
                    {
                        level = LogLevel.None;
                        break;
                    }
                    case TraceLevel.Verbose:
                    {
                        level = LogLevel.Debug;
                        break;
                    }
                    case TraceLevel.Warning:
                    {
                        level = LogLevel.Warning;
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                switch (info.TraceInfoStep)
                {
                    case TraceInfoStep.BeforeExecute:
                    {
                        _logger.Log(level, info.SqlText);
                        break;
                    }
                    case TraceInfoStep.AfterExecute:
                    {
                        _logger.Log(
                            level,
                            "SQL Execution completed (AfterExecute): {0}",
                            info.ExecutionTime);
                        break;
                    }
                    case TraceInfoStep.Error:
                    {
                        _logger.Log(level, 0, info.Exception, "Exception occured during SQL execution");
                        break;
                    }
                    case TraceInfoStep.MapperCreated:
                        break;
                    case TraceInfoStep.Completed:
                    {
                        _logger.Log(
                            level,
                            "SQL Execution finished. DataReader closed (Completed): {0}",
                            info.ExecutionTime);
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            };
#endif
        }

        public ITable<GoogleApiSettings> GoogleApiSettings => GetTable<GoogleApiSettings>();
        public ITable<Product> Products => GetTable<Product>();
        public ITable<ProductImage> ProductImages => GetTable<ProductImage>();
        public ITable<SearchTask> SearchTasks => GetTable<SearchTask>();
        public ITable<SelectedProductImage> SelectedProductImages => GetTable<SelectedProductImage>();

        public ITable<TempProduct> TempProducts => GetTable<TempProduct>();
    }
}