﻿using FluentMigrator.Runner.VersionTableInfo;
using Infrastructure.DataAccess.PostgreSQL.FluentMigrator;

namespace Web.External.ImageSearch.Application.DataAccess.Migrations
{
    [VersionTableMetaData]
    public class VersionTableMetadata : PostgreSqlVersionTableMetadata
    {
    }
}