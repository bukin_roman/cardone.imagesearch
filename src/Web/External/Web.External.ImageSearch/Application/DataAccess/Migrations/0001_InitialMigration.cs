﻿using System.Data;
using System.Diagnostics.CodeAnalysis;
using FluentMigrator;

namespace Web.External.ImageSearch.Application.DataAccess.Migrations
{
    [Migration(1)]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class _0001_InitialMigration : Migration
    {
        // @formatter:off
        public override void Up()
        {
            Create.Table("google_api_settings")
                .WithColumn("id")
                    .AsInt64()
                    .NotNullable()
                    .Identity()
                    .PrimaryKey("google_api_settings_pkey")
                .WithColumn("custom_search_engine_id")
                    .AsString(255)
                    .NotNullable()
                .WithColumn("google_api_key")
                    .AsString(255)
                    .NotNullable()
                .WithColumn("daily_request_limit")
                    .AsInt32()
                    .NotNullable()
                .WithColumn("last_fail_date")
                    .AsDateTimeOffset()
                    .Nullable()
                .WithColumn("creation_date")
                    .AsDateTimeOffset()
                    .NotNullable()
                    .WithDefault(SystemMethods.CurrentUTCDateTime);

            Create.Index("google_api_settings_custom_search_engine_id_google_api_key_idx")
                .OnTable("google_api_settings")
                .OnColumn("custom_search_engine_id").Ascending()
                .OnColumn("google_api_key").Ascending()
                .WithOptions()
                .Unique();

            Create.Table("product")
                .WithColumn("product_id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey("product_pkey")
                .WithColumn("vendor_code")
                    .AsString(255)
                    .NotNullable()
                .WithColumn("manufacturer_name")
                    .AsString(128)
                    .NotNullable()
                .WithColumn("title")
                    .AsString(4000)
                    .NotNullable()
                .WithColumn("creation_date")
                    .AsDateTimeOffset()
                    .NotNullable()
                    .WithDefault(SystemMethods.CurrentUTCDateTime);

            Create.Table("search_task")
                .WithColumn("id")
                    .AsInt64()
                    .NotNullable()
                    .Identity()
                    .PrimaryKey("search_task_pkey")
                .WithColumn("google_api_settings_id")
                    .AsInt64()
                    .NotNullable()
                    .ForeignKey("search_task_google_api_settings_id_fkey", "google_api_settings", "id")
                        .OnDelete(Rule.Cascade)
                        .OnUpdate(Rule.Cascade)
                .WithColumn("product_id")
                    .AsInt64()
                    .NotNullable()
                    .ForeignKey("search_task_product_id_fkey", "product", "product_id")
                        .OnDelete(Rule.Cascade)
                        .OnUpdate(Rule.Cascade)
                .WithColumn("last_execution_attempt_date")
                    .AsDateTimeOffset()
                    .Nullable()
                .WithColumn("status")
                    .AsInt32()
                    .NotNullable()
                    .WithDefaultValue(0)
                .WithColumn("creation_date")
                    .AsDateTimeOffset()
                    .NotNullable()
                    .WithDefault(SystemMethods.CurrentUTCDateTime);

            Create.Table("product_image")
                .WithColumn("id")
                    .AsInt64()
                    .NotNullable()
                    .Identity()
                    .PrimaryKey("product_image_pkey")
                .WithColumn("search_task_id")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("product_image_search_task_id_fkey", "search_task", "id")
                        .OnDelete(Rule.SetNull)
                        .OnUpdate(Rule.Cascade)
                .WithColumn("product_id")
                    .AsInt64()
                    .NotNullable()
                    .ForeignKey("product_image_product_id_fkey", "product", "product_id")
                        .OnDelete(Rule.Cascade)
                        .OnUpdate(Rule.Cascade)
                .WithColumn("url")
                    .AsString(4000)
                    .NotNullable();

            Create.Table("selected_product_image")
                .WithColumn("product_id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey("selected_product_image_pkey")
                    .ForeignKey("selected_product_image_product_id_fkey", "product", "product_id")
                        .OnDelete(Rule.Cascade)
                        .OnUpdate(Rule.Cascade)
                .WithColumn("selected_image_id")
                    .AsInt64()
                    .NotNullable()
                    .ForeignKey("selected_product_image_selected_image_id_fkey", "product_image", "id")
                        .OnDelete(Rule.Cascade)
                        .OnUpdate(Rule.Cascade)
                .WithColumn("author_id")
                    .AsInt64()
                    .Nullable()
                .WithColumn("creation_date")
                    .AsDateTimeOffset()
                    .NotNullable()
                    .WithDefault(SystemMethods.CurrentUTCDateTime);
        }
        // @formatter:on

        public override void Down()
        {
            Delete.Table("selected_product_image");
            Delete.Table("product_image");
            Delete.Table("search_task");
            Delete.Table("product");
            Delete.Table("google_api_settings");
        }
    }
}