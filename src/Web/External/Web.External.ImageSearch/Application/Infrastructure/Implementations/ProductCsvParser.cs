﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using CsvHelper;
using Web.External.ImageSearch.Application.Infrastructure.Abstractions.ProductCsvParser;

namespace Web.External.ImageSearch.Application.Infrastructure.Implementations
{
    public class ProductCsvParser : IProductCsvParser
    {
        private static readonly CsvHelper.Configuration.Configuration Configuration =
            new CsvHelper.Configuration.Configuration
            {
                AllowComments = false,
                CultureInfo = CultureInfo.InvariantCulture,
                Delimiter = ";",
                Quote = '"',
                Encoding = Encoding.UTF8,
                BadDataFound = ctx => { },
                LineBreakInQuotedFieldIsBadData = true,
                IgnoreQuotes = true
            };

        private static readonly ExpectedCsvStructure ExpectedCsvStructure = ReflectExpectedStructure();

        public ExpectedCsvStructure GetExpectedStructure()
        {
            return ExpectedCsvStructure;
        }

        public IEnumerable<ProductInfo> Parse(Stream csvStream)
        {
            if (csvStream == null)
                throw new ArgumentNullException(nameof(csvStream));
            using (var textReader = new StreamReader(csvStream, Encoding.UTF8, true, 1024, true))
            using (var csv = new CsvReader(textReader, Configuration, true))
            {
                while (csv.Read())
                    if (csv.TryGetField<long>(0, out var id)
                        && id > 0L
                        && csv.TryGetField<string>(1, out var csvVendorCode) && !string.IsNullOrEmpty(csvVendorCode)
                        && csv.TryGetField<string>(2, out var csvManufacturer) && !string.IsNullOrEmpty(csvManufacturer)
                        && csv.TryGetField<string>(3, out var csvTitle) && !string.IsNullOrEmpty(csvTitle))
                    {
                        var vendorCode = NormalizeString(csvVendorCode).Normalize().ToUpperInvariant();
                        var manufacturer = NormalizeString(csvManufacturer);
                        var title = NormalizeString(csvTitle);

                        if (vendorCode.Length <= 255
                            && manufacturer.Length <= 255
                            && title.Length <= 4000)
                            yield return new ProductInfo(
                                id,
                                vendorCode,
                                manufacturer,
                                title);
                    }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string NormalizeString(string stringToNormalize)
        {
            return stringToNormalize.Trim().Trim('\'', '"', ',', '.', '`', ':', ';').Trim();
        }

        private static ExpectedCsvStructure ReflectExpectedStructure()
        {
            var encoding = Configuration.Encoding.WebName;
            var delimiter = Configuration.Delimiter;
            var quote = Configuration.Quote;
            var hasHeaderRecord = Configuration.HasHeaderRecord;

            var expectedColumns = typeof(ProductInfo)
                .GetProperties()
                .Select(x =>
                {
                    var propertyType = x.PropertyType;
                    var nullable = false;
                    if (propertyType.IsGenericType &&
                        propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        propertyType = propertyType.GetGenericArguments()[0];
                        nullable = true;
                    }

                    var propertyTypeName = propertyType.Name;
                    return new ExpectedCsvStructureColumn(x.Name, propertyTypeName, nullable);
                }).ToArray();

            return new ExpectedCsvStructure(
                encoding,
                delimiter,
                quote,
                hasHeaderRecord,
                expectedColumns);
        }
    }
}