﻿using System;

namespace Web.External.ImageSearch.Application.Infrastructure.Abstractions.ProductCsvParser
{
    public class ProductInfo
    {
        public ProductInfo(long productId, string vendorCode, string manufacturerName, string title)
        {
            if (productId <= 0)
                throw new ArgumentOutOfRangeException(nameof(productId));
            if (string.IsNullOrEmpty(vendorCode))
                throw new ArgumentNullException(nameof(vendorCode));
            if (string.IsNullOrEmpty(manufacturerName))
                throw new ArgumentNullException(nameof(manufacturerName));
            if (string.IsNullOrEmpty(title))
                throw new ArgumentNullException(nameof(title));
            ProductId = productId;
            VendorCode = vendorCode;
            ManufacturerName = manufacturerName;
            Title = title;
        }

        public long ProductId { get; }

        public string VendorCode { get; }

        public string ManufacturerName { get; }

        public string Title { get; }
    }
}