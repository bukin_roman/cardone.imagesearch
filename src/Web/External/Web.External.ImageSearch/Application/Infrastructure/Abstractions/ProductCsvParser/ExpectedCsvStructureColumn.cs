﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.External.ImageSearch.Application.Infrastructure.Abstractions.ProductCsvParser
{
    /// <summary>
    ///     Колонка, ожидаемого csv-файла.
    /// </summary>
    public class ExpectedCsvStructureColumn
    {
        public ExpectedCsvStructureColumn(
            string name,
            string type,
            bool nullable)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrEmpty(type))
                throw new ArgumentNullException(nameof(type));
            Name = name;
            Type = type;
            Nullable = nullable;
        }

        /// <summary>
        ///     Наименование колонки.
        /// </summary>
        [Required]
        public string Name { get; }

        /// <summary>
        ///     Тип данных колонки.
        /// </summary>
        [Required]
        public string Type { get; }

        /// <summary>
        ///     Может ли значение быть null.
        /// </summary>
        [Required]
        public bool Nullable { get; }
    }
}