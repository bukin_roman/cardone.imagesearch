﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Web.External.ImageSearch.Application.Infrastructure.Abstractions.ProductCsvParser
{
    /// <summary>
    ///     Ожидаемая структура CSV-файла.
    /// </summary>
    public class ExpectedCsvStructure
    {
        public ExpectedCsvStructure(
            string encoding,
            string delimiter,
            char quote,
            bool hasHeaderRecord,
            ExpectedCsvStructureColumn[] columns)
        {
            if (string.IsNullOrEmpty(encoding))
                throw new ArgumentNullException(nameof(encoding));
            if (string.IsNullOrEmpty(delimiter))
                throw new ArgumentNullException(nameof(delimiter));
            Encoding = encoding;
            Delimiter = delimiter;
            Quote = quote;
            HasHeaderRecord = hasHeaderRecord;
            if (columns?.Length > 0)
                Columns = columns.Where(x => x != null).ToArray();
        }

        /// <summary>
        ///     Кодировка.
        /// </summary>
        [Required]
        public string Encoding { get; }

        /// <summary>
        ///     Разделитель записей.
        /// </summary>
        [Required]
        public string Delimiter { get; }

        /// <summary>
        ///     Символ кавычек.
        /// </summary>
        [Required]
        public char Quote { get; }

        /// <summary>
        ///     Должен ли файл содержать заголовки.
        /// </summary>
        [Required]
        public bool HasHeaderRecord { get; }

        /// <summary>
        ///     Колонки файла.
        /// </summary>
        [Required]
        public ExpectedCsvStructureColumn[] Columns { get; } = { };
    }
}