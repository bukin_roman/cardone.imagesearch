﻿using System.Collections.Generic;
using System.IO;

namespace Web.External.ImageSearch.Application.Infrastructure.Abstractions.ProductCsvParser
{
    /// <summary>
    ///     Парсер CSV-файлов.
    /// </summary>
    public interface IProductCsvParser
    {
        /// <summary>
        ///     Парсит поток, содержащий csv-файл, извлекая оттуда информацию о товарах.
        /// </summary>
        /// <param name="csvStream">Поток, содержащий csv-файл c информацией о товарах.</param>
        /// <returns></returns>
        IEnumerable<ProductInfo> Parse(Stream csvStream);

        /// <summary>
        ///     Возвращает информацию о структуре файла, которую ожидает парсер csv-файлов..
        /// </summary>
        /// <returns></returns>
        ExpectedCsvStructure GetExpectedStructure();
    }
}