﻿using System.ComponentModel.DataAnnotations;

namespace Web.External.ImageSearch.WebModels.GoogleApiSettings.Requests
{
    /// <summary>
    ///     Базовая модель запроса.
    /// </summary>
    public abstract class BaseGoogleApiSettingsRequestModel
    {
        /// <summary>
        ///     Идентификатор Custom Search Engine.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string CustomSearchEngineId { get; set; }

        /// <summary>
        ///     Ключ Google API.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string GoogleApiKey { get; set; }

        /// <summary>
        ///     Ограничение на количество поисковых запросов в сутки.
        /// </summary>
        [Range(1L, long.MaxValue)]
        public long DailyRequestLimit { get; set; }
    }
}