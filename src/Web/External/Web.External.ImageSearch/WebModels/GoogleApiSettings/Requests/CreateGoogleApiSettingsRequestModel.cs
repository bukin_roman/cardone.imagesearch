﻿namespace Web.External.ImageSearch.WebModels.GoogleApiSettings.Requests
{
    /// <summary>
    ///     Запрос на создание настроек для работы с Google API.
    /// </summary>
    public class CreateGoogleApiSettingsRequestModel : BaseGoogleApiSettingsRequestModel
    {
    }
}