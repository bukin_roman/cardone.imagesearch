﻿namespace Web.External.ImageSearch.WebModels.GoogleApiSettings.Requests
{
    /// <summary>
    ///     Запрос на обновление настроек для работы с Google API.
    /// </summary>
    public class UpdateGoogleApiSettingsRequestModel : BaseGoogleApiSettingsRequestModel
    {
    }
}