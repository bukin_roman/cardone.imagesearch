﻿using System;

namespace Web.External.ImageSearch.WebModels.GoogleApiSettings.Responses
{
    /// <summary>
    ///     Параметры для работы с Google API.
    /// </summary>
    public class GoogleApiSettingsResponseModel
    {
        /// <summary>
        ///     Конструирует параметры для работы с Google API.
        /// </summary>
        /// <param name="id">Уникальный идентификатор конкретного набора параметров.</param>
        /// <param name="customSearchEngineId">Идентификатор Custom Search Engine.</param>
        /// <param name="googleApiKey">Ключ Google API.</param>
        /// <param name="dailyRequestLimit">Ограничение на количество поисковых запросов в сутки.</param>
        /// <param name="creationDate">Дата создания.</param>
        public GoogleApiSettingsResponseModel(
            long id,
            string customSearchEngineId,
            string googleApiKey,
            long dailyRequestLimit,
            DateTimeOffset creationDate)
        {
            Id = id;
            CustomSearchEngineId = customSearchEngineId;
            GoogleApiKey = googleApiKey;
            DailyRequestLimit = dailyRequestLimit;
            CreationDate = creationDate;
        }

        /// <summary>
        ///     Уникальный идентификатор конкретного набора параметров.
        /// </summary>
        public long Id { get; }

        /// <summary>
        ///     Идентификатор Custom Search Engine.
        /// </summary>
        public string CustomSearchEngineId { get; }

        /// <summary>
        ///     Ключ Google API.
        /// </summary>
        public string GoogleApiKey { get; }

        /// <summary>
        ///     Ограничение на количество поисковых запросов в сутки.
        /// </summary>
        public long DailyRequestLimit { get; }

        /// <summary>
        ///     Дата создания.
        /// </summary>
        public DateTimeOffset CreationDate { get; }
    }
}