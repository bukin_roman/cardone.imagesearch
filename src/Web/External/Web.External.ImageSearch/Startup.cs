﻿using System;
using System.Net.Http;
using System.Reflection;
using FluentMigrator.Runner;
using Hangfire;
using IdentityServer4.AccessTokenValidation;
using Infrastructure.AutoMapper.Extensions.Installers;
using Infrastructure.DataAccess.PostgreSQL.FluentMigrator.Extensions.Installers;
using Infrastructure.Web.Api.Extensions.Installers;
using Infrastructure.Web.Api.Extensions.Swagger;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using Web.External.ImageSearch.Application.DataAccess;
using Web.External.ImageSearch.Application.Infrastructure.Abstractions.ProductCsvParser;
using Web.External.ImageSearch.Application.Infrastructure.Implementations;
using Web.External.ImageSearch.Application.Services.Abstractions;
using Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService;
using Web.External.ImageSearch.Application.Services.Abstractions.SearchService;
using Web.External.ImageSearch.Application.Services.Implementations;
using Web.External.ImageSearch.Application.Services.Implementations.SearchService;
using Web.External.ImageSearch.Configuration;
using Web.External.ImageSearch.Extensions;
using Web.External.ImageSearch.Extensions.Installers;

namespace Web.External.ImageSearch
{
    public class Startup
    {
        private const string SwaggerPath = "image-search";
        private const string SwaggerTitle = "Image Search";
        private static readonly Assembly CurrentAssembly = typeof(Startup).Assembly;
        private static readonly string SwaggerApiVersion = CurrentAssembly.GetName().Version.ToString(3);
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var appConnectionString = _configuration.GetConnectionString("ImageSearchDatabase");
            var hangfireConnectionString = _configuration.GetConnectionString("ImageSearchHangfireDatabase");

            services
                .InstallDataProtection(
                    "Web.External.ImageSearch",
                    _configuration.GetValue<string>("DataProtectionDirectory"))
                .InstallMvcForApi(options =>
                    {
                        options.Filters.Add(new AuthorizeFilter());
                        options.Filters.Add(new ProducesResponseTypeAttribute(401));
                        options.Filters.Add(new ProducesResponseTypeAttribute(403));
                    },
                    CurrentAssembly)
                .InstallAuthorization(ConfigureAuthorization)
                .InstallClientAuthentication(_configuration.GetSection("IdentityServerAuthenticationOptions"))
                .InstallCorsServices(_configuration.GetValue<string[]>("CorsAllowedDomains"))
                .InstallLocalization()
                .InstallSwagger(
                    SwaggerPath,
                    SwaggerTitle,
                    SwaggerApiVersion,
                    new[] {CurrentAssembly},
                    OAuth2SchemeFactory.Create(
                        _configuration.GetValue<string>("IdentityServerAuthenticationOptions:Authority"),
                        Constants.Identity.Scope,
                        Constants.Identity.ScopeDescription))
                .InstallPostgresFluentMigrator(
                    appConnectionString,
                    CurrentAssembly)
                .InstallAutomapper(typeof(Startup))
                .InstallHangfire(hangfireConnectionString);
            services.Configure<FormOptions>(options =>
            {
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = long.MaxValue;
            });
            services.AddSingleton<IProductCsvParser, ProductCsvParser>();
            services.AddScoped<IProductImportService, ProductImportService>();
            services.AddScoped<IImageSearchPlanningService, ImageSearchPlanningService>();
            services.AddScoped<IGoogleApiSettingsService, GoogleApiSettingsService>();
            services.AddScoped(resolver =>
            {
                var logger = resolver.GetRequiredService<ILogger<ImageSearchConnection>>();
                return new ImageSearchConnection(appConnectionString, logger);
            });
            services.AddHttpClient(nameof(GoogleSearchServiceFactory), options =>
                {
                    options.Timeout = TimeSpan.FromSeconds(60);
                    options.BaseAddress = new Uri("https://www.googleapis.com", UriKind.Absolute);
                })
                .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
                {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(5),
                    TimeSpan.FromSeconds(10)
                }));
            services.AddScoped<IGoogleSearchServiceFactory>(resolver =>
            {
                var factory = resolver.GetRequiredService<IHttpClientFactory>();
                var httpClient = factory.CreateClient(nameof(GoogleSearchServiceFactory));
                return new GoogleSearchServiceFactory(httpClient);
            });
        }

        private static void ConfigureAuthorization(AuthorizationOptions options)
        {
            var builder = new AuthorizationPolicyBuilder(IdentityServerAuthenticationDefaults.AuthenticationScheme);
            var policy = builder
                .RequireAuthenticatedUser()
                .RequireScope(Constants.Identity.Scope)
                .Build();
            options.AddPolicy("DefaultAuthorizationPolicy", policy);
            options.DefaultPolicy = policy;
        }

        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            IMigrationRunner runner)
        {
            runner.MigrateUp();

            app.ConfigureHangfireJobs();
            app.UseHangfireDashboard();
            app.UseHangfireServer();

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/error");
            app.UseRequestLocalization();
            app.UseAuthentication();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(
                    $"/swagger/{SwaggerPath}/swagger.json",
                    SwaggerTitle);
                options.OAuthClientId("swagger_image_search");
            });
            app.UseSwagger();
            app.UseMvc();
        }
    }
}