﻿using System;
using System.IO;
using System.Reflection;
using Infrastructure.DataAccess.PostgreSQL.Linq2Db;
using Infrastructure.Web.Api.Extensions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Web.External.ImageSearch
{
    public static class Program
    {
        private static readonly IConfiguration GlobalLoggerConfiguration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", false, true)
            .AddJsonFile(
                $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json",
                true,
                true)
            .AddEnvironmentVariables()
            .Build();

        public static void Main(string[] args)
        {
            using (var mainLogger = new LoggerConfiguration()
                .ReadFrom.Configuration(GlobalLoggerConfiguration)
                .CreateLogger())
            {
                try
                {
                    mainLogger.Information("Starting web host");
                    CreateWebHostBuilder(args).Build().Run();
                }
                catch (Exception ex)
                {
                    mainLogger.Fatal(ex, "Host terminated unexpectedly");
                }
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            CodePages.Register();
            CustomPostgresDataProvider.Register();
            return WebHost
                .CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var hostingEnvironment = hostingContext.HostingEnvironment;
                    if (hostingEnvironment.IsEnvironment("ef") || hostingEnvironment.IsDevelopment())
                    {
                        var assembly = Assembly.Load(new AssemblyName(hostingEnvironment.ApplicationName));
                        if (assembly != null)
                            config.AddUserSecrets(assembly, false);
                    }
                })
                .ConfigureLogging(builder => builder.ClearProviders())
                .UseSerilog((hostingContext, loggerConfiguration) =>
                    loggerConfiguration.ReadFrom.Configuration(hostingContext.Configuration))
                .UseStartup<Startup>();
        }
    }
}