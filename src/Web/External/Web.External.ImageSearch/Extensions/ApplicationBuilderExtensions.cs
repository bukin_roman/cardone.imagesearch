﻿using System;
using System.Linq;
using Hangfire;
using Hangfire.Common;
using Hangfire.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Web.External.ImageSearch.Hangfire.Abstractions;

namespace Web.External.ImageSearch.Extensions
{
    /// <summary>
    ///     Методы расширения для <see cref="IApplicationBuilder" />
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        ///     Выполняет настройку заданий Hangfire.
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder ConfigureHangfireJobs(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
                var serviceProvider = scope.ServiceProvider;
                var repeatableJobs = serviceProvider.GetServices<IAsyncRepeatableHangfireJob>().ToArray();
                var recurringJobManager = serviceProvider.GetRequiredService<IRecurringJobManager>();
                foreach (var repeatableJob in repeatableJobs)
                {
                    var jobType = repeatableJob.GetType();
                    var method = jobType.GetMethod(nameof(IAsyncRepeatableHangfireJob.RunJob));
                    recurringJobManager.AddOrUpdate(
                        repeatableJob.GetName(),
                        new Job(jobType, method, null as PerformContext),
                        repeatableJob.GetCronExpression(),
                        timeZone);
                }
            }

            return app;
        }
    }
}