﻿using System;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.Extensions.DependencyInjection;
using Web.External.ImageSearch.Hangfire.Abstractions;

namespace Web.External.ImageSearch.Extensions.Installers
{
    /// <summary>
    ///     Методы расширения для установки и настройки сервисов, необходимых для работы Hangfire.
    /// </summary>
    public static class HangfireInstaller
    {
        /// <summary>
        ///     Устанавливает и настраивает сервисы, необходимые для работы Hangfire.
        /// </summary>
        /// <param name="services">Экземпляр настраиваемой коллекции сервисов.</param>
        /// <param name="hangfireDatabaseConnectionString">Строка соединения с базой данных Hangfire.</param>
        /// <returns></returns>
        public static IServiceCollection InstallHangfire(
            this IServiceCollection services,
            string hangfireDatabaseConnectionString)
        {
            return services
                .AddHangfire(config =>
                {
                    config.UsePostgreSqlStorage(
                        hangfireDatabaseConnectionString,
                        new PostgreSqlStorageOptions
                        {
                            PrepareSchemaIfNecessary = true, // Инициализируем схему.
                            // Таймаут выполнения тасок (как долго Hangfire будет считать их живыми)
                            InvisibilityTimeout = TimeSpan.FromHours(23)
                        });
                    config.UsePostgreSqlMetrics();
                })
                .InstallHangfireJobs();
        }

        /// <summary>
        ///     Регистрирует задачи для Hangfire в IoC контейнере.
        /// </summary>
        /// <param name="services">Экземпляр настраиваемой коллекции сервисов.</param>
        /// <returns></returns>
        private static IServiceCollection InstallHangfireJobs(this IServiceCollection services)
        {
            services.Scan(x =>
                x.FromEntryAssembly()
                    .AddClasses(c => c.AssignableTo<IAsyncRepeatableHangfireJob>(), false)
                    .AsImplementedInterfaces()
                    .AsSelf()
                    .WithScopedLifetime()
            );
            return services;
        }
    }
}