﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Infrastructure.Web.Api.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Annotations;
using Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService;
using Web.External.ImageSearch.Application.Services.Abstractions.GoogleApiSettingsService.Model;
using Web.External.ImageSearch.WebModels.GoogleApiSettings.Requests;
using Web.External.ImageSearch.WebModels.GoogleApiSettings.Responses;

namespace Web.External.ImageSearch.Controllers
{
    /// <summary>
    ///     Контроллер настроек для взаимодействия с Google API.
    /// </summary>
    [ApiController]
    [Route("api/settings/google")]
    [Produces("application/json")]
    public class GoogleApiSettingsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGoogleApiSettingsService _settingsService;

        public GoogleApiSettingsController(IMapper mapper, IGoogleApiSettingsService settingsService,
            IOptionsSnapshot<ApiBehaviorOptions> opts)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _settingsService = settingsService ?? throw new ArgumentNullException(nameof(settingsService));
        }

        /// <summary>
        ///     Возвращает все существующие настройки.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены выполнения запроса.</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<GoogleApiSettingsResponseModel>), 200)]
        [SwaggerOperation(OperationId = "GetAll")]
        public async Task<IEnumerable<GoogleApiSettingsResponseModel>> GetAllAsync(
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var allSettings = await _settingsService.GetAllAsync(cancellationToken);
            var resultSettings = _mapper.Map<IEnumerable<GoogleApiSettingsResponseModel>>(allSettings);
            return resultSettings;
        }

        /// <summary>
        ///     Возвращает настройки по их уникальному идентификатору.
        /// </summary>
        /// <param name="settingsId">Уникальный идентификатор настроек.</param>
        /// <param name="cancellationToken">Токен отмены выполнения запроса.</param>
        /// <returns></returns>
        [HttpGet("{settingsId:long:min(1)}")]
        [ProducesResponseType(typeof(GoogleApiSettingsResponseModel), 200)]
        [ProducesResponseType(404)]
        [SwaggerOperation(OperationId = "FindById")]
        public async Task<ActionResult<GoogleApiSettingsResponseModel>> FindByIdAsync(
            [FromRoute] long settingsId,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var apiSettings = await _settingsService.FindAsync(settingsId, cancellationToken);
            if (apiSettings == null)
                return NotFound();
            var resultSettings = _mapper.Map<GoogleApiSettingsResponseModel>(apiSettings);
            return Ok(resultSettings);
        }

        /// <summary>
        ///     Создаёт настройки для работы с Google API.
        /// </summary>
        /// <param name="model">Запрос на создание настроек для работы с Google API.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(GoogleApiSettingsResponseModel), 201)]
        [SwaggerOperation(OperationId = "Create")]
        public async Task<ActionResult> CreateAsync(
            CreateGoogleApiSettingsRequestModel model,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            await Task.Delay(10, cancellationToken);
            var settingsToCreate = _mapper.Map<GoogleApiSettingsCreateModel>(model);
            var createdSettings = await _settingsService.CreateAsync(settingsToCreate, cancellationToken);
            var resultSettings = _mapper.Map<GoogleApiSettingsResponseModel>(createdSettings);
            return CreatedAtAction(nameof(FindByIdAsync), new
            {
                settingsId = createdSettings.Id
            }, resultSettings);
        }

        /// <summary>
        ///     Обновляет настройки для работы с Google API.
        /// </summary>
        /// <param name="settingsId">Уникальный идентификатор обновляемых настроек.</param>
        /// <param name="model">Запрос на обновление настроек для работы с Google API.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.</param>
        /// <returns></returns>
        [HttpPut("{settingsId:long:min(1)}")]
        [ProducesResponseType(typeof(GoogleApiSettingsResponseModel), 200)]
        [ProducesResponseType(404)]
        [SwaggerOperation(OperationId = "Update")]
        [Consumes("application/json")]
        [ObjectNotFoundExceptionFilter]
        public async Task<GoogleApiSettingsResponseModel> UpdateAsync(
            [FromRoute] long settingsId,
            [FromBody] UpdateGoogleApiSettingsRequestModel model,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var settingsToUpdate = _mapper.Map<GoogleApiSettingsUpdateModel>(model);
            var updatedSettings = await _settingsService.UpdateAsync(
                settingsId,
                settingsToUpdate,
                cancellationToken);
            var resultSettings = _mapper.Map<GoogleApiSettingsResponseModel>(updatedSettings);
            return resultSettings;
        }

        /// <summary>
        ///     Удаляет настройки для работы с Google API.
        /// </summary>
        /// <param name="settingsId">Уникальный идентификатор удаляемых настроек.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.</param>
        /// <returns></returns>
        [HttpDelete("{settingsId:long:min(1)}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [SwaggerOperation(OperationId = "Delete")]
        [ObjectNotFoundExceptionFilter]
        public async Task<ActionResult> DeleteAsync(
            [FromRoute] long settingsId,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            await _settingsService.DeleteAsync(settingsId, cancellationToken);
            return Ok();
        }
    }
}