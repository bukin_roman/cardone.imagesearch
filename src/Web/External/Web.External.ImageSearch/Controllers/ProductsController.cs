﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Web.External.ImageSearch.Application.Infrastructure.Abstractions.ProductCsvParser;
using Web.External.ImageSearch.Application.Model;
using Web.External.ImageSearch.Application.Services.Abstractions;

namespace Web.External.ImageSearch.Controllers
{
    /// <summary>
    ///     Контроллер товаров.
    /// </summary>
    [ApiController]
    [Route("api/products")]
    [Produces("application/json")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductCsvParser _parser;
        private readonly IProductImportService _productService;

        public ProductsController(
            IProductCsvParser parser,
            IProductImportService productService)
        {
            _parser = parser ?? throw new ArgumentNullException(nameof(parser));
            _productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        /// <summary>
        ///     Возвращает информацию о структуре ожидаемого csv-файла.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены выполнения запроса.</param>
        /// <returns></returns>
        [HttpGet("import/csv/structure")]
        [ProducesResponseType(typeof(ExpectedCsvStructure), 200)]
        [SwaggerOperation(OperationId = "GetCsvStructure")]
        public ActionResult<ExpectedCsvStructure> GetCsvStructure(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var structure = _parser.GetExpectedStructure();
            return Ok(structure);
        }

        /// <summary>
        ///     Импортирует товары из csv-файла.
        /// </summary>
        /// <param name="file">csv-файл, содержащий товары.</param>
        /// <param name="cancellationToken">Токен отмены выполнения команды.</param>
        /// <returns></returns>
        [HttpPost("import/csv")]
        [ProducesResponseType(200)]
        [RequestSizeLimit(long.MaxValue)]
        [SwaggerOperation(OperationId = "ImportProductsFromCsv")]
        public async Task<ActionResult> ImportProductsFromCsv(
            [Required] IFormFile file,
            CancellationToken cancellationToken)
        {
            if (Path.GetExtension(file.FileName)?.ToUpperInvariant() != ".CSV")
                throw new InvalidOperationException();
            var products = _parser.Parse(file.OpenReadStream());
            await _productService.ImportProductsAsync(
                products.Select(x => new Product
                {
                    ProductId = x.ProductId,
                    VendorCode = x.VendorCode,
                    ManufacturerName = x.ManufacturerName,
                    Title = x.Title
                }),
                cancellationToken);
            return new StatusCodeResult(200);
        }
    }
}